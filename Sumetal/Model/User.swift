//
//  User.swift
//  Sumetal
//
//  Created by Thiago Santos on 06/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

struct User {
    var id : Int = 0
    var nome: String = ""
    var userName: String = ""
    var email: String = ""
    var datanascimento: String = ""
    var password: String = ""
    var cnpj: String = ""
    var fantasia: String = ""
    var inscricao: String = ""
    var endereco: String = ""
    var logradouro: String = ""
    var estado: String = ""
    var cidade: String = ""
    var cep: String = ""
    var telefone: String = ""
    
    init() {}
    init(json: JSON) {
        self.id             = json["id"].intValue
        self.nome           = json["nome"].stringValue
        self.userName       = json["username"].stringValue
        self.email          = json["email"].stringValue
        self.datanascimento = json["datanascimento"].stringValue
        self.password       = json["pass"].stringValue
        if self.password.isEmpty {
            self.password     = json["password"].stringValue
        }
        
        self.cnpj           = json["cnpj"].stringValue
        self.fantasia       = json["fantasia"].stringValue
        self.inscricao      = json["inscricao"].stringValue
        self.endereco       = json["endereco"].stringValue
        self.logradouro     = json["logradouro"].stringValue
        self.estado         = json["estado"].stringValue
        self.cidade         = json["cidade"].stringValue
        self.cep            = json["cep"].stringValue
        self.telefone       = json["telefone"].stringValue
        self.userName       = json["user"].stringValue
        
     

    }
}
