//
//  Categoria.swift
//  Sumetal
//
//  Created by Thiago Santos on 10/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Categoria {
    var id: Int = 0
    var nome = ""
    init() {}
    public typealias blockCompletion = (Categoria?, NSError?) -> Void
    init(json: JSON) {
        self.id = json["id"].intValue
        self.nome = json["nome"].stringValue
    }
    
    
}
