//
//  Pedido.swift
//  Sumetal
//
//  Created by Thiago Santos on 16/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreData

struct Item {
    var nome:       String = ""
    var ref:        String = ""
    var produto:    Int = 0
    var imagem:     String = ""
    var qtd:        String = ""
    var valor:      String = ""
    var total:      String = ""
    init() {
        
    }
    init(itens:  Itens){
        self.nome = itens.nome ?? ""
        self.ref = itens.ref ?? ""
        self.produto = Int(itens.produto)
        self.imagem = itens.imagem ?? ""
        self.qtd = "\(itens.qtd)"
        if let valor = itens.valor{
            self.valor = "\(String(describing: valor))"
        }
        
    }
    init(json: JSON) {
       
        self.nome       = json["nome"].stringValue
        self.ref        = json["ref"].stringValue
        self.produto    = json["produto"].intValue
        self.imagem     = json["imagem"].stringValue
        self.qtd        = json["qtd"].stringValue
        self.valor      = json["valor"].stringValue
        self.total      = json["total"].stringValue
    }
}
