//
//  Produto.swift
//  Sumetal
//
//  Created by Thiago Santos on 07/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import CoreData

struct Produto {
    var id:         Int  = 0
    var thumb:      String = ""
    var nome:       String = ""
    var categoria:  String = ""
    var preco:      String = ""
    var tamanho:    String = ""
    var dimensao:   String = ""
    var formato:    String = ""
    var peso:       String = ""
    var correia:    String = ""
    var acabamento: String = ""
    var fundo:      String = ""
    var fabricacao: String = ""
    var fixacao   : String = ""
    var modalidade: String = ""
    var codigo    : String = ""
    var subcategoria:  String = ""
    
    
    init() {}
    init(core: ProdutoManager) {
        self.dimensao       = core.dimensao!
        self.thumb          = core.thumb!
        self.acabamento     = core.acabamento!
        self.codigo         = core.codigo!
        self.categoria      = core.categoria!
        self.correia        = core.correia!
        self.fabricacao     = core.fabricacao!
        self.fixacao        =  core.fixacao!
        self.formato        = core.formato!
        self.fundo          = core.fundo!
        self.id             = Int(core.id)
        self.modalidade     = core.modalidade!
        self.nome           = core.nome!
        self.peso           = core.peso!
        self.preco          = core.preco!
        self.subcategoria   = core.subcategoria!
        self.tamanho        = core.tamanho!
    }
    
    init(json: JSON) {
        self.id             = json["id"].intValue
        self.thumb          = json["thumb"].stringValue
        self.nome           = json["nome"].stringValue
        self.categoria      = json["categoria"].stringValue
        self.preco          = json["preco"].stringValue
        self.tamanho        = json["tamanho"].stringValue
        self.dimensao       = json["dimensao"].stringValue
        self.formato        = json["formato"].stringValue
        self.peso           = json["peso"].stringValue
        self.correia        = json["correia"].stringValue
        self.acabamento     = json["acabamento"].stringValue
        self.fundo          = json["fundo"].stringValue
        self.fabricacao     = json["fabricacao"].stringValue
        self.fixacao        = json["fixacao"].stringValue
        self.modalidade     = json["modalidade"].stringValue
        self.codigo         = json["codigo"].stringValue
        self.subcategoria   = json["tipoproduto"].stringValue
    }
}
