//
//  Pedido.swift
//  Sumetal
//
//  Created by Thiago Santos on 21/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Pedido {
    var id : Int = 0
    var itens: [Item] = []
    var nomeCliente = ""
    var obs = ""
    
    var condicoesPagamento = ""
    var valorBruto = ""
    var valorLiquido = ""
    var valorDesconto = ""
    var condicoes = ""
    var dataPedido = ""
    var status = ""
    init(){}
    
    init(json: JSON) {
        
        self.id = json["id"].intValue
        self.nomeCliente = json["comprador"].stringValue
        self.obs = json["obs"].stringValue
        self.dataPedido = json["data"].stringValue
        self.valorLiquido = json["valor"].stringValue
        if self.valorLiquido == ""{
            self.valorLiquido = json["bruto"].stringValue
        }
        self.condicoesPagamento = json["pagamento"].stringValue
         self.status = json["status"].stringValue
        self.valorDesconto = json["desconto"].stringValue
        self.valorBruto = json["bruto"].stringValue
        self.obs = json["obs"].stringValue
        
    }
}
