//
//  APICache.swift
//  Sumetal
//
//  Created by Thiago Santos on 18/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import Foundation
import CoreData

class APICache: NSManagedObject {
   @NSManaged  var id: String
   @NSManaged  var cacheDuration: Int
   @NSManaged  var json: Data
   @NSManaged  var date: Date
   @NSManaged  var offlineExpiresIn: Date
   @NSManaged  var onlineExpiresIn: Date
 
    
    
}
class APICacheDuration {
    
    /// Default cache duration (zero). Without cache.
    public static let zero = APICacheDuration(online: 0.0, offline: 0.0)
    
    public fileprivate(set) var online: TimeInterval   = 0.0
    public fileprivate(set) var offline: TimeInterval  = 0.0 // Existe por conta de uma lógica no Telecine que quando estiver sem internet o cache é maior (Assista Offline)
    
    public var hasOnlineCache: Bool { return online > 0 }
    public var hasOfflineCache: Bool { return offline > 0 }
    
    
    public init(online: TimeInterval, offline: TimeInterval) {
        self.online = online
        self.offline = offline
     
        
    }
    
    /// Generate online expiration date from Now
    public func onlineExpirationDate() -> Date {
        return getExpiresIn(seconds: online)
    }
    
    /// Generate offline expiration date from Now
    public func offlineExpirationDate() -> Date {
        return getExpiresIn(seconds: offline)
    }
    
    fileprivate func getExpiresIn(seconds sec: TimeInterval) -> Date {
        return Date().addingTimeInterval(sec)
    }
    
}
