//
//  APIClient.swift
//  Sumetal
//
//  Created by Thiago Santos on 06/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Haneke
import QHashString
import CoreData


// MARK: - ApiURL

internal struct APIURL: URLConvertible {
    
    var url: String
    
    init(urlString: String) {
        url = urlString
    }
    
    public func asURL() throws -> URL {
        return URL(string: url)!
    }
}

// MARK: - String(Hash)

extension String {
    var hashString: String { return self.q_sha512() }
}

/**
 Request status code
 
 - Ok
 - Created
 - Accepted
 - NoContent
 - MovedPermanently
 - Found
 - NotModified
 - BadRequest
 - Unauthorized
 - Forbidden
 - NotFound
 - MethodNotAllowed
 - RequestTimeout
 - InternalServerError
 - NotImplemented
 - BadGateway
 - ServiceUnavailable
 - GatewayTimeout
 */
public struct APIStatusCode {
    static let Ok = 200
    static let Created = 201
    static let Accepted = 202
    static let NoContent = 204
    static let MovedPermanently = 301
    static let Found = 302
    static let NotModified = 304
    static let BadRequest = 400
    static let Unauthorized = 401
    static let Forbidden = 403
    static let NotFound = 404
    static let MethodNotAllowed = 405
    static let RequestTimeout = 408
    static let InternalServerError = 500
    static let NotImplemented = 501
    static let BadGateway = 502
    static let ServiceUnavailable = 503
    static let GatewayTimeout = 504
}

class APIClient {
    
    
    static let shared =  APIClient()
    internal let manager: SessionManager = SessionManager()
       
    
    func login(user: String, pass: String, completionHandler: @escaping (_ result: User? , _ error: Error?) -> ())  {
        let url = App.baseURL+"login.php"
        
        let param :  Parameters = [ "user": user, "pass": pass ]
       _ = Alamofire.request(url, method: .post, parameters: param).validate().responseJSON {  response in
        
            switch response.result {
                case .success:
                    if let jsonObject = response.result.value {
                        let json                = JSON(jsonObject).arrayValue
                       
                        if let users = json.first {
                          
                            guard users["msg"] == "Nenhum usuário encontrado" else{
                                
                                let user     = User(json: users)
                                
                                completionHandler(user, nil)
                                return
                            }
                            let error = NSError(domain: "com.Ativa.Sumetal", code: 20, userInfo: [NSLocalizedDescriptionKey: "Nenhum usuário encontrado"])
                            completionHandler(nil, error)
                            
                        }
                }
                case let .failure(error):
                    let  erros =  self.msgErros(error: error, response: response.response)
                    completionHandler(nil, erros)
            }
        
        }
        
    }
    func cadastroUser(user: User, completionHandler: @escaping(_ result: Bool , _ error: Error?) ->()){
        
        let url = App.baseURL+"createuser.php"
        let param :  Parameters = [
            "nome": user.nome,
            "endereco": user.endereco,
            "telefone": user.telefone,
            "cidade": user.cidade,
            "estado": user.estado,
            "email": user.email,
            "datanascimento": user.datanascimento,
            "username": user.userName,
            "password": user.password,
            "logradouro": user.endereco,
            "fantasia": user.fantasia,
            "cnpj": user.cnpj,
            "inscriacao": user.inscricao
            
        ]
        _ = Alamofire.request(url, method: .post, parameters: param).validate().responseJSON { response in
            switch response.result {
            case .success:
                if let jsonObject = response.result.value {
                    let json                = JSON(jsonObject).arrayValue.first
                    
                    guard let _json = json else{
                        completionHandler(false, nil)
                        return
                    }
                    
                    if  _json["msg"].stringValue == "success" {
                        completionHandler(true, nil)
                        return
                    }
                    completionHandler(false, nil)
                    
                }
            case let .failure(error):
                let  erros =  self.msgErros(error: error, response: response.response)
                completionHandler(false, erros)
            }
        }
    }
    func deleteAllCarrinho(usuario: String, completionHandler: @escaping(_ status: Bool) -> ()) {
        let url = App.baseURL+"cancelar.php"
        let param :  Parameters = [ "usuario": usuario ]
        
        _ = Alamofire.request(url, method: .post, parameters: param).validate().responseJSON { response in
            
            switch response.result {
                case .success:
                    completionHandler(true)
                case .failure(_):
                  completionHandler(false)
            }
            
        }
        
    }
    func sendPedidos(_ pedido : Pedido, completionHandler: @escaping(_ status: Bool, _ error: Error?) -> ())  {
        guard let _user = myAppDelegate.getUser() else{return}
        
        
        
        
        let param :  Parameters = [
            "usuario": _user.userName,
            "datapedido":pedido.dataPedido,
            "nomecliente": pedido.nomeCliente,
            "pagamento": pedido.condicoesPagamento,
            "valordesconto": 0,
            "condicoes": pedido.condicoes,
            "obs":pedido.obs,
            "bruto": pedido.valorBruto.floatValue.currencyBR,
            "liquido": pedido.valorBruto.floatValue.currencyBR
            
        ]
        print(param)
        let url = App.baseURL+"pedidos.php"
        _ = Alamofire.request(url, method: .post , parameters: param).validate().responseJSON { response in
            switch response.result {
            case .success:
                if let jsonObject = response.result.value {
                    let json          = JSON(jsonObject).arrayValue.first
                    guard let _json = json else{
                        completionHandler(false , nil)
                        return
                    }
                    
                    
                    if  _json["message"].stringValue == "success" {
                        completionHandler(true, nil)
                        return
                    }
                    completionHandler(false, nil)
                }
            case .failure(let error):
                completionHandler(false, error)
                break
            }
        }
    }
    func deleteCarrinho(codigo: Int, completionHandler: @escaping(_ status: Bool) -> ()) {
        let url = App.baseURL+"remove.php"
        let param :  Parameters = [ "cod": codigo ]
        
        _ = Alamofire.request(url, method: .post, parameters: param).validate().responseJSON { response in
            
            switch response.result {
            case .success:
                if let jsonObject = response.result.value {
                    let json                = JSON(jsonObject).arrayValue.first
                    guard let _json = json else{
                        completionHandler(false)
                        return
                    }
                    
                    if  _json["msg"].stringValue == "success" {
                        completionHandler(true)
                        return
                    }
                     completionHandler(false)

                }
                completionHandler(true)
            case .failure(let error):
                print(error)
                completionHandler(false)
            }
            
        }
        
    }
    func updateUser(user: User, completionHandler: @escaping(_ result: Bool , _ error: Error?) ->()){
        
        let url = App.baseURL+"updateuser.php"
        let param :  Parameters = [
            "nome": user.nome,
            "endereco": user.endereco,
            "telefone": user.telefone,
            "cidade": user.cidade,
            "estado": user.estado,
            "email": user.email,
            "datanascimento": user.datanascimento,
            "username": user.userName,
            "password": user.password,
            "logradouro": user.endereco,
            "fantasia": user.fantasia,
            "cnpj": user.cnpj,
            "inscriacao": user.inscricao
            
        ]
        _ = Alamofire.request(url, method: .post, parameters: param).validate().responseJSON { response in
            switch response.result {
            case .success:
                if let jsonObject = response.result.value {
                    let json                = JSON(jsonObject).arrayValue.first
                    
                    guard let _json = json else{
                        completionHandler(false, nil)
                        return
                    }
                    
                    if  _json["msg"].stringValue == "success" {
                        completionHandler(true, nil)
                        return
                    }
                    completionHandler(false, nil)
                    
                }
            case .failure(let error):
                let  erros =  self.msgErros(error: error, response: response.response)
                completionHandler(false, erros)
                break
            }
            
        }
        
    }
    func featchCarrinho(vendedor: String, completionHandler: @escaping(_ Item: [Item]?, _ error: Error?) -> ()){
        let url = App.baseURL+"carrinho.php"
         guard let _user = myAppDelegate.getUser() else{return}
        let param :  Parameters = [ "vendedor": _user.userName ]
        
        _ = Alamofire.request(url, method: .post, parameters: param).validate().responseJSON { response in
            
            switch response.result {
            case .success:
                if let jsonObject = response.result.value {
                    let json                = JSON(jsonObject).arrayValue
                    var items            = [Item]()
                    items                =  json.map({return Item(json: $0)})
                   
                     completionHandler(items, nil)
                }
                break
            case .failure(let error):
                let  erros =  self.msgErros(error: error, response: response.response)
                completionHandler(nil, erros)
                break
            }
            
        }
    }
    func featchPedidoDados(id: Int, completionHandler: @escaping(_ pedido: Pedido?, _ error: Error?) -> ()){
        let url = App.baseURL+"dadospedido.php"
        guard let _user = myAppDelegate.getUser() else{return}
        let param :  Parameters = [
            "idvenda":id,
            "vendedor": _user.userName ]
        
        
        _ = Alamofire.request(url, method: .post , parameters: param).validate().responseJSON { response in
            switch response.result {
            case .success:
                if let jsonObject = response.result.value {
                    let json                = JSON(jsonObject).arrayValue
                    print(json)
                    var pedido              = [Pedido]()
                     pedido                 = json.map({return Pedido(json: $0)})
                    completionHandler(pedido.first, nil)
                }
                
            case .failure(let error):
                let  erros =  self.msgErros(error: error, response: response.response)
                completionHandler(nil, erros)
                break
                
            }
        }
       
    }
    func featchPedidoItem(id: Int, completionHandler: @escaping(_ itens: [Item]?, _ error: Error?) -> ()){
        let url = App.baseURL+"itenspedido.php"
         guard let _user = myAppDelegate.getUser() else{return}
        let param :  Parameters = [
            "idvenda":id,
            "vendedor": _user.userName ]
        
        
        _ = Alamofire.request(url, method: .get , parameters: param).validate().responseJSON { response in
            switch response.result {
            case .success:
                if let jsonObject = response.result.value {
                    let json                = JSON(jsonObject).arrayValue
                    var items            = [Item]()
                    items                =  json.map({return Item(json: $0)})
                    completionHandler(items, nil)
                }
                
            case .failure(let error):
                let  erros =  self.msgErros(error: error, response: response.response)
                completionHandler(nil, erros)
                break
            
            }
        }
    }
    func featchProdutoAll(completionHandler: @escaping(_ produtos: [Produto]?, _ error: Error?) -> ()){
//        let url = App.baseURLDef+"api/get_allproducts/"
        
        
        //Make the request
      
        let url  = App.baseURLDef+"/api/get_allproductsjson"
        
        
        _ = AFManager.request(url, method: .post).validate().responseJSON { response in
            
            switch response.result {
            case .success:
                
                if let jsonObject = response.result.value {
                    
                    let json                = JSON(jsonObject)
                    var produtos            = [Produto]()
                    let arrayProduto        =  json["produtos"].arrayValue
                    produtos                =  arrayProduto.map({return Produto(json: $0)})
                    completionHandler(produtos, nil)
                }
                break
            case .failure(let error):
                completionHandler(nil, error)
                break
            }
            
        }
    }
    func produtofilter(categoria: String, subCategoria: String, completionHandler: @escaping(_ produtos: [Produto]?, _ error: Error?) -> ()){
        let url = App.baseURLDef+"api/get_page_params"
        let param :  Parameters = [ "tipo": categoria , "produto": subCategoria]
        _ = Alamofire.request(url, method: .get , parameters: param).validate().responseJSON { response in
            switch response.result {
            case .success:
                if let jsonObject = response.result.value {
                   let json                = JSON(jsonObject).arrayValue
                   var produtos            = [Produto]()
                       produtos            = json.map({return Produto(json: $0)})
                    let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
                        completionHandler(produtos, nil)
                    })
                }
            case .failure(let error):
                let  erros =  self.msgErros(error: error, response: response.response)
                completionHandler(nil, erros)
                break
            }
        }
    }
    
    func produtoID(id: Int, completionHandler: @escaping(_ produto: Produto?, _ error: Error?) -> ()){
        let url = App.baseURLDef+"api/get_post_unico"
        let param :  Parameters = [ "id": id]
        _ = Alamofire.request(url, method: .get , parameters: param).validate().responseJSON { response in
            switch response.result {
            case .success:
                if let jsonObject = response.result.value {
                    let json                = JSON(jsonObject).arrayValue
                    var produtos            = Produto()
                    produtos                = json.map({return Produto(json: $0)}).first!
                    produtos.id = id
                 
                    completionHandler(produtos, nil)
                }
            case .failure(let error):
              completionHandler(nil, error)
                break
            }
        }
    }
    func feacthPedidos(_ vendedor : String, completionHandler: @escaping(_ pedidos: [Pedido]?, _ error: Error?) -> ())  {
        guard let _user = myAppDelegate.getUser() else{return}
        let param :  Parameters = [ "vendedor": _user.userName]
        let url = App.baseURL+"pedido.php"
        _ = Alamofire.request(url, method: .post , parameters: param).validate().responseJSON { response in
            switch response.result {
            case .success:
                if let jsonObject = response.result.value {
                    let json                = JSON(jsonObject).arrayValue
                    var pedidos            = [Pedido]()
                    pedidos                = json.map({return Pedido(json: $0)})
                    
                    completionHandler(pedidos, nil)
                }
            case .failure(let error):
                completionHandler(nil, error)
                break
            }
        }
    }
  
    
    func sendItens(_ itens : Item, completionHandler: @escaping(_ status: Bool, _ error: Error?) -> ()) {
        let url = App.baseURL+"itens.php"
        guard let _user = myAppDelegate.getUser() else{return}
        let param : Parameters  = [
            "nome":itens.nome,
            "referencia": itens.ref,
            "produto":"\(itens.produto)",
            "imagem":itens.imagem,
            "quantidade":itens.qtd,
            "preco":itens.valor,
            "vendedor": _user.userName
        ]
       

       
        _ = Alamofire.request(url, method: .post, parameters: param).validate().responseJSON { response in
            switch response.result {
             case .success:
                if let jsonObject = response.result.value {
                    let json                = JSON(jsonObject).arrayValue.first
                    
                    guard let _json = json else{
                        completionHandler(false, nil)
                        return
                    }
                    
                    if  _json["msg"].stringValue == "success" {
                        completionHandler(true, nil)
                        return
                    }
                    completionHandler(false, nil)
                    
                }
            case .failure(let error):
               let  erros =  self.msgErros(error: error, response: response.response)
                completionHandler(false, erros)
                break
            }
            
        }
        
    }
    func fetchProfile(id: Int, completionHandler:@escaping(_ user: User?, _ error: Error?) -> ()){
        let url = App.baseURL+"profileuser.php"
        guard let _user = myAppDelegate.getUser() else{return}
        let param :  Parameters = [ "id": _user.id]
        _ = Alamofire.request(url, method: .post , parameters: param).validate().responseJSON { response in
            switch response.result {
            case .success:
                if let jsonObject = response.result.value {
                      let json                = JSON(jsonObject).arrayValue
                      var users               = [User]()
                        users                 = json.map({return User(json: $0)})
                    completionHandler(users.first, nil)
                    
                }
                case .failure(let error):
                  let  erros =  self.msgErros(error: error, response: response.response)
                    completionHandler(nil, erros)
                break
            }
        }
    }
    func fetchClientes(user: String, completionHandler:@escaping(_ user: [User]?, _ error: Error?) -> ()){
        let url = App.baseURL+"clientes.php"
         guard let _user = myAppDelegate.getUser() else{return}
        let param :  Parameters = [ "user": _user.userName, "opcao": "listar", "search": ""]
        _ = Alamofire.request(url, method: .post , parameters: param).validate().responseJSON { response in
            
            switch response.result {
            case .success:
                if let jsonObject = response.result.value {
                    let json                = JSON(jsonObject).arrayValue
                    var users               = [User]()
                    users                 = json.map({return User(json: $0)})
                    completionHandler(users, nil)
                    
                }
            case .failure(let error):
                let  erros =  self.msgErros(error: error, response: response.response)
                completionHandler(nil, erros)
                break
            }
        }
    }
    func msgErros(error: Error, response: HTTPURLResponse?) -> Error {
    
        if error.localizedDescription == "The Internet connection appears to be offline."{
            let error = NSError(domain: "com.Ativa.Sumetal", code: 10, userInfo: [NSLocalizedDescriptionKey: "Verifica sua conexão com á internet"])
           
            return error
        }
        return error
    }
   
}

// MARK: - Error

extension NSError {
    open static func errorWithCode(_ code: Int, failureReason: String) -> NSError {
        let userInfo = [NSLocalizedFailureReasonErrorKey: failureReason]
        return NSError(domain: "com.Ativa.Sumetal", code: code, userInfo: userInfo)
    }
}
