//
//  PedidosCell.swift
//  Sumetal
//
//  Created by Thiago Santos on 16/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class PedidosCell: UITableViewCell {
    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
