//
//  ClientesManager.swift
//  Sumetal
//
//  Created by Thiago Santos on 28/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import Foundation
import CoreData

class ClientesManager: NSManagedObject {
    
    static func featchClientes() -> [Clientes]?{
        let predicate = NSPredicate(value: true)
        let ordeby = NSSortDescriptor(key: "cnpj", ascending: true)
        
        let cdManager = CoreDataManager.sharedInstance
        guard  let _itens =  cdManager.customSearch("Clientes",  predicate: predicate, sortDescriptor: ordeby) as? [Clientes] else {
            
            return nil
        }
        return _itens
    }
    static func featchUser () -> [User]?{
        var users = [User]()
        guard let _client =  ClientesManager.featchClientes() else {
            return nil
        }
        for _cliente in _client{
            var user = User()
            user.cep    =  _cliente.cep ?? ""
            user.cidade = _cliente.cidade ?? ""
            user.cnpj   = _cliente.cnpj ?? ""
            user.estado = _cliente.estado ?? ""
            user.nome   = _cliente.nome ?? ""
            user.endereco   = _cliente.endereco ?? ""
            user.telefone = _cliente.telefone ?? ""
            
            
            users.append(user)
        }
        
        return users
    }
}
