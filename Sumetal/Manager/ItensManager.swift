//
//  ItensManager.swift
//  Sumetal
//
//  Created by Thiago Santos on 23/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import Foundation
import CoreData

class ItensManager: NSManagedObject {
    
    
      @NSManaged var produto: Int
      @NSManaged var nome: String
      @NSManaged var ref: String
      @NSManaged var qtd: Int
      @NSManaged var imagem: String
      @NSManaged var valor: String
    
    
    
     static func valorTotal() -> Int{
        let predicate = NSPredicate(value: true)
        let ordeby = NSSortDescriptor(key: "produto", ascending: true)
        
        let cdManager = CoreDataManager.sharedInstance
        guard  let _itens =  cdManager.customSearch("Itens",  predicate: predicate, sortDescriptor: ordeby) as? [Itens] else {
            
            return 0
        }
        var total = 0
        
        
            for item in _itens {
                
                if let valor = item.valor{
                    total  += Int(item.qtd) * valor.intValue
                 
                }
          
            }
        return total
    }
    static func featchItem() -> [Item]?{
        let predicate = NSPredicate(value: true)
        let ordeby = NSSortDescriptor(key: "produto", ascending: true)
        
        let cdManager = CoreDataManager.sharedInstance
        guard  let _itens =  cdManager.customSearch("Itens",  predicate: predicate, sortDescriptor: ordeby) as? [Itens] else {
            
            return nil
        }
        return _itens.map({return Item(itens: $0)})
    }
    static func featchItens() -> [Itens]?{
        let predicate = NSPredicate(value: true)
        let ordeby = NSSortDescriptor(key: "produto", ascending: true)
        
        let cdManager = CoreDataManager.sharedInstance
        guard  let _itens =  cdManager.customSearch("Itens",  predicate: predicate, sortDescriptor: ordeby) as? [Itens] else {
            
            return nil
        }
        return _itens
    }
    static func delete(_ item: Itens) -> Bool {
        let cdManager = CoreDataManager.sharedInstance
         cdManager.deleteObj(item)
        
        return true
    }
    static func delete(_ item: Item){
        let cdManager = CoreDataManager.sharedInstance
        
        // Create a sort descriptor object that sorts on the "title"
        // property of the Core Data object
        let sortDescriptor = NSSortDescriptor(key: "nome", ascending: true)
       
        
        // Create a new predicate that filters out any object that
        // doesn't have a title of "Best Language" exactly.
        let predicate = NSPredicate(format: "nome == %@", item.nome)
        
       
        guard  let _itens =  cdManager.customSearch("Itens", predicate: predicate, sortDescriptor: sortDescriptor) as? [Itens] else {
            
            return
        }
        
        if let  _item  = _itens.filter({ return $0.nome == item.nome }).first {
            cdManager.deleteObj(_item)
        }
        
        
     
    }
}
