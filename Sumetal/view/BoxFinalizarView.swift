//
//  BoxFinalizarView.swift
//  Sumetal
//
//  Created by Thiago Santos on 22/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit


class BoxFinalizarView: UIView, ReusableView {

    var delegate : BoxFinalizarViewDelegate?


    class func instanciateFromNib() -> BoxFinalizarView {
        return Bundle.main.loadNibNamed("BoxFinalizar", owner: nil, options: nil)![0] as! BoxFinalizarView
    }
    @IBAction func sendNext(_ sender: Any) {
        delegate?.nextCompra(pedido: Pedido())
    }
    @IBAction func tapComprar(_ sender: Any) {
        delegate?.prevCompra()
       
    }
    
    
    
}
