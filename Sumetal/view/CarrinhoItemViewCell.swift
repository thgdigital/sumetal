//
//  CarrinhoItemViewCell.swift
//  Sumetal
//
//  Created by Thiago Santos on 27/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import SDWebImage

protocol CarrinhoDelegate {
    func remover(_ index:Int)
}

class CarrinhoItemViewCell: UICollectionViewCell {
    var delegate: CarrinhoDelegate?
    var item: Item? {
        didSet{
            if let titulo = item?.nome {
                titleLabel?.text = titulo
            }
            
            if let image = item?.imagem{
               
                cardImageView?.sd_setImage(with: URL(string: image), placeholderImage: #imageLiteral(resourceName: "logo-place"))
            }
            if let qtd = item?.qtd, let valor = item?.valor{
                subTitleLabel?.text = "QTD: \(qtd)  Valor: R$ \(valor)"
            }
          
        }
    }
    
    
    @IBOutlet weak var separador: UIView!

    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func removerCell(_ sender: Any) {
        guard let _item = self.item else{return}
        
        UIAlertController.showAlert(title: "Atenção !!", message: "Deseja remover este item \n \(_item.nome)", cancelButtonTitle:"cancelar", confirmationButtonTitle: "OK", dissmissBlock: {
            if let id = self.item?.produto{
                self.delegate?.remover(id)
            }
        }, cancelBlock: nil)
        
       
    }

}
