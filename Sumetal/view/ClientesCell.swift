//
//  ClientesCell.swift
//  Sumetal
//
//  Created by Thiago Santos on 12/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class ClientesCell: UITableViewCell {
    var clientes : User?{
        didSet{
            self.setupLayout()
        }
    }

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var endLabel: UILabel!
    
    func setupLayout(){
        guard let _clintes = clientes else {return}
            titleLabel.text = _clintes.nome.uppercased()
            subTitleLabel.text  = _clintes.endereco
            endLabel.text = "\(_clintes.cidade) - \(_clintes.estado) - \(_clintes.telefone) - \(_clintes.cnpj)"
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    class func instanciateFromNib() -> ClientesCell {
                return Bundle.main.loadNibNamed(defaultReuseIdentifier, owner: nil, options: nil)![0] as! ClientesCell
    }
    
}
