//
//  SubcatCell.swift
//  Sumetal
//
//  Created by Thiago Santos on 12/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class SubcatCell: UITableViewCell {

    var title: String? {
        didSet{
            if let titulo = title {
                titleLabel?.text = titulo
            }
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
       backgroundColor =    App.color
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
