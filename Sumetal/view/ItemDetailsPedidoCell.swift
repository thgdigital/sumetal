//
//  ItemDetailsPedidoCell.swift
//  Sumetal
//
//  Created by Thiago Santos on 27/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import SDWebImage

class ItemDetailsPedidoCell: UITableViewCell {
    var item : Item?{
        didSet{
            if let imagem =  item?.imagem{
                cardImageview.sd_setImage(with: URL(string: imagem), placeholderImage: #imageLiteral(resourceName: "logo-place"))
            }
            if let qtd = item?.qtd, let preco = item?.valor{
                subTitle.text = "QTD: \(qtd)  Valor: R$ \(preco)"
            }
            if let nome = item?.nome{
                titleLabel.text = nome
            }
        }
    }
    
    @IBOutlet weak var cardImageview: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
