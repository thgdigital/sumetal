//
//  ProdutoCell.swift
//  Sumetal
//
//  Created by Thiago Santos on 11/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import SDWebImage

class ProdutoCell: UITableViewCell {
    @IBOutlet weak var imageDestaqueImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var produto: Produto? {
        didSet{
            if let titulo = produto?.nome {
                titleLabel?.text = titulo
            }
            if let image = produto?.thumb{
               
                imageDestaqueImageView.sd_setImage(with: URL(string: image), placeholderImage: #imageLiteral(resourceName: "logo-place"))
            }
         
        }
    }


//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//        
//        imageDestaqueImageView?.layer.cornerRadius = 5
//        imageDestaqueImageView?.contentMode = .scaleAspectFill
//        imageDestaqueImageView?.clipsToBounds = true
//    }
//     class func instanciateFromNib() -> ProdutoCell {
//        return Bundle.main.loadNibNamed(defaultReuseIdentifier, owner: nil, options: nil)![0] as! ProdutoCell
//    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
