//
//  FormPedidoCell.swift
//  Sumetal
//
//  Created by Thiago Santos on 19/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

protocol BoxFinalizarViewDelegate {
    func prevCompra()
    func nextCompra(pedido: Pedido)
}

class FormPedidoCell: UITableViewCell  {
    
    var delegate : BoxFinalizarViewDelegate?

    var pickerArray = ["Dinheiro", "Cartão"]
    
    var item: Item? {
        didSet{
            if let valor = item?.total{
                valortotalLabel.text = "R$ \(valor)"
                obsTextView.text = ""
                textePekvIEW.text = "Forma de Pagamento"
                condicoesLabel.text = ""
            }
        }
    }

    @IBOutlet weak var obsTextView: UITextView!
    @IBOutlet weak var condicoesLabel: UITextField!
    @IBOutlet weak var textePekvIEW: UILabel!
    @IBOutlet weak var valortotalLabel: UILabel!
    
    @IBOutlet weak var fakerView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        pickerView.isHidden = true
         let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(FormPedidoCell.showPicker))
        fakerView.addGestureRecognizer(tap)
        pickerView.dataSource = self
        pickerView.delegate = self
        condicoesLabel.delegate = self
        let tapTeclado: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(FormPedidoCell.dismissKeyboard))
        self.addGestureRecognizer(tapTeclado)
//        obsTextView.delegate = self
    }
    func dismissKeyboard() {
        self.endEditing(true)
    }
    
    func setupLayout(){
        let total = ItensManager.valorTotal()
        valortotalLabel.text = total.floatvalue.currencyBR
        obsTextView.text = ""
        textePekvIEW.text = "Forma de Pagamento"
        condicoesLabel.text = ""
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func showPicker(){
        UIView.animate(withDuration: 0.4) { 
            self.pickerView.isHidden = false
        }
        
   
        self.layoutIfNeeded()
        self.setNeedsDisplay()
    }
    
    @IBAction func sendNext(_ sender: Any) {
        guard  let textpike = textePekvIEW.text , let condicoes =  condicoesLabel.text,textpike != "", condicoes != "" else {
                UIAlertController.showAlert(title: "Opss error", message: "existem dados vazios", cancelButtonTitle: "Ok", cancelBlock: nil)
            return
          }
        let total = ItensManager.valorTotal()
        var pedido = Pedido()
        pedido.obs = obsTextView.text
        pedido.condicoesPagamento = textpike
        pedido.condicoes = condicoes
        pedido.valorBruto = "\(total)"
        pedido.valorDesconto = "0"
        pedido.valorLiquido = "\(total)"
        
        
        
        delegate?.nextCompra(pedido: pedido)
    }
    @IBAction func tapComprar(_ sender: Any) {
        delegate?.prevCompra()
        
    }
    
}
extension FormPedidoCell:UIPickerViewDataSource, UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        textePekvIEW.text = pickerArray[row]
        UIView.animate(withDuration: 0.4) {
            self.pickerView.isHidden = true
        }
    }
    
}
extension FormPedidoCell: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        textField.resignFirstResponder()
        return true
    }
}
