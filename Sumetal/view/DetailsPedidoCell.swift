//
//  DetailsPedidoCell.swift
//  Sumetal
//
//  Created by Thiago Santos on 27/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class DetailsPedidoCell: UITableViewCell {
    
    
    
    
    
    var pedido : Pedido?{
        didSet{
            if let date = pedido?.dataPedido{
                dateLabel.text = "Data do Pedido: \(date)"
            }
            if let cliente = pedido?.nomeCliente{
                clienteLabel.text = "Cliente:  \(cliente)"
            }
            if let valorBruto = pedido?.valorBruto{
                valorBrutoLabel.text = "Valor Bruto:  \(valorBruto)"
            }
            if let desconto  = pedido?.valorDesconto{
                descontoLabel.text = "Desconto: \(desconto)"
            }
            if let forma = pedido?.condicoesPagamento {
                formaLabel.text = "Forma de Pagamento:  \(forma)"
            }
            if let valorliquido = pedido?.valorLiquido {
                valorliquidolabel.text = "Valor Liquido:  \(valorliquido)"
            }
            if let status = pedido?.status{
                statusLabel.text = "Status: \(status)"
            }
        
        }
    }

    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var clienteLabel: UILabel!
    
    @IBOutlet weak var valorBrutoLabel: UILabel!
    
    @IBOutlet weak var descontoLabel: UILabel!
    
    @IBOutlet weak var formaLabel: UILabel!
    
    
    @IBOutlet weak var valorliquidolabel: UILabel!
    
    @IBOutlet weak var statusLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
