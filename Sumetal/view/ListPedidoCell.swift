//
//  ListPedidoCell.swift
//  Sumetal
//
//  Created by Thiago Santos on 24/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class ListPedidoCell: UITableViewCell {
    var pedido :Pedido?{
        didSet{
            self.setup()
        }
    }

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var subTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setup(){
        guard let _pedidos = pedido else{return}
        titleLabel.text = _pedidos.nomeCliente
        subTitle.text   = "\(_pedidos.dataPedido)   \(_pedidos.valorLiquido)"
    }

}
