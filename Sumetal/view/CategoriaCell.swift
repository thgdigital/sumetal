//
//  CategoriaCell.swift
//  Sumetal
//
//  Created by Thiago Santos on 10/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class CategoriaCell: UITableViewCell {

    @IBOutlet weak var nameTexField: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
