//
//  ItemPedidoCell.swift
//  Sumetal
//
//  Created by Thiago Santos on 19/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class ItemPedidoCell: UITableViewCell {
    
    var item: Item? {
        didSet{
            if let titulo = item?.nome {
                titleLabel?.text = titulo
            }
           
            if let image = item?.imagem{
                produtoImageView.sd_setImage(with: URL(string: image), placeholderImage: #imageLiteral(resourceName: "logo-place"))
            }
            if let qtd = item?.qtd{
                subTitleLabel?.text = "QTD: \(qtd)"
            }
            if let valor = item?.valor {
                refLabel?.text =  "Valor: R$ \(valor)"
            }
        }
    }
    @IBOutlet weak var refLabel: UILabel!
    @IBOutlet weak var produtoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
