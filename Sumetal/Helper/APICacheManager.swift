//
//  APICacheManager.swift
//  Sumetal
//
//  Created by Thiago Santos on 18/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import CoreData


// MARK: - CacheManager

public struct APICacheManager {
    let cdManager = CoreDataManager.sharedInstance
    
    
    static func save(requestUrl: String, sectionName: String, data: AnyObject, headersResponse: Any? = nil) {
        
        save(requestUrl: requestUrl, sectionName: "teste", data: data)
        
    }

    
   static func getCacheObject(_ path: String) -> APICache? {
 
    
    // Create a sort descriptor object that sorts on the "title"
    // property of the Core Data object
    let sortDescriptor = NSSortDescriptor(key: "id", ascending: true)

    
    // Create a new predicate that filters out any object that
    // doesn't have a title of "Best Language" exactly.
    let predicate = NSPredicate(format: "id == %@", path.hashString)
    print(path.hashString)
    let cdManager = CoreDataManager.sharedInstance
    guard  let cache =  cdManager.customSearch("Cache", predicate: predicate, sortDescriptor: sortDescriptor)?.first as? APICache else {
        
        return nil
    }
    

        return cache
    }
    func save(requestUrl: String, json: AnyObject, cacheDuration: APICacheDuration){
         let jsonData: Data = json as! Data
          let newCache = cdManager.new("Cache") as! APICache
            newCache.id = requestUrl.hashString
            newCache.json = jsonData
            newCache.offlineExpiresIn = cacheDuration.offlineExpirationDate()
            newCache.onlineExpiresIn = cacheDuration.onlineExpirationDate()
            cdManager.saveObj(newCache)
    }
}
