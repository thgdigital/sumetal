//
//  Helper.swift
//  Sumetal
//
//  Created by Thiago Santos on 04/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import Foundation
import UIKit
import ReachabilitySwift
import FontAwesome_swift
import M13BadgeView

let myAppDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate

struct App {
    
    static let color = UIColor.rgb(red: 0, green: 117, blue: 179)
    static let baseURL = "http://www.sumetal.com.br/_app/ios/"
    static let baseURLDef = "http://www.sumetal.com.br/"
    static func getUser() -> String {
         let defaults = UserDefaults.standard
        guard  defaults.object(forKey: "user") != nil else {
            return ""
        }
    
        return ""
    }
}

// MARK: - UIColor
extension UIColor {
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
}
extension Double {
    var CGFloatValue: CGFloat {
        get {
            return CGFloat(self)
        }
    }
}
extension Int {
    var CGFloatValue: CGFloat {
        get {
            return CGFloat(self)
        }
    }
    var floatvalue: Float{
        get {
            return Float(self)
        }
    
    }
}
public class Utils: NSObject {
    
    public static let reachability: Reachability? = Reachability()
    
    public class func hasConnection() -> Bool {
        
        if let reachability = reachability {
            return reachability.isReachable
        }
        return false
    }
    
    public class func isCarrier() -> Bool{
        
        if let reachability = reachability {
            return reachability.isReachableViaWWAN
        }
        return false
    }
    
    open class func isWiFi() -> Bool{
        
        if let reachability = reachability {
            return reachability.isReachableViaWiFi
        }
        return false
    }
    public static func isValidEmail(email:String) -> Bool {
        print("validate emilId: \(email)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
}
extension UIView {
    public func addConstraintsWithFormat(_ format: String, views: UIView...) {
        
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
    public func fillSuperview() {
        translatesAutoresizingMaskIntoConstraints = false
        if let superview = superview {
            leftAnchor.constraint(equalTo: superview.leftAnchor).isActive = true
            rightAnchor.constraint(equalTo: superview.rightAnchor).isActive = true
            topAnchor.constraint(equalTo: superview.topAnchor).isActive = true
            bottomAnchor.constraint(equalTo: superview.bottomAnchor).isActive = true
        }
    }
    
    public func anchor(_ top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil, topConstant: CGFloat = 0, leftConstant: CGFloat = 0, bottomConstant: CGFloat = 0, rightConstant: CGFloat = 0, widthConstant: CGFloat = 0, heightConstant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        
        _ = anchorWithReturnAnchors(top, left: left, bottom: bottom, right: right, topConstant: topConstant, leftConstant: leftConstant, bottomConstant: bottomConstant, rightConstant: rightConstant, widthConstant: widthConstant, heightConstant: heightConstant)
    }
    
    public func anchorWithReturnAnchors(_ top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil, topConstant: CGFloat = 0, leftConstant: CGFloat = 0, bottomConstant: CGFloat = 0, rightConstant: CGFloat = 0, widthConstant: CGFloat = 0, heightConstant: CGFloat = 0) -> [NSLayoutConstraint] {
        translatesAutoresizingMaskIntoConstraints = false
        
        var anchors = [NSLayoutConstraint]()
        
        if let top = top {
            anchors.append(topAnchor.constraint(equalTo: top, constant: topConstant))
        }
        
        if let left = left {
            anchors.append(leftAnchor.constraint(equalTo: left, constant: leftConstant))
        }
        
        if let bottom = bottom {
            anchors.append(bottomAnchor.constraint(equalTo: bottom, constant: -bottomConstant))
        }
        
        if let right = right {
            anchors.append(rightAnchor.constraint(equalTo: right, constant: -rightConstant))
        }
        
        if widthConstant > 0 {
            anchors.append(widthAnchor.constraint(equalToConstant: widthConstant))
        }
        
        if heightConstant > 0 {
            anchors.append(heightAnchor.constraint(equalToConstant: heightConstant))
        }
        
        anchors.forEach({$0.isActive = true})
        
        return anchors
    }
    
    public func anchorCenterXToSuperview(constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        if let anchor = superview?.centerXAnchor {
            centerXAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        }
    }
    
    public func anchorCenterYToSuperview(constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        if let anchor = superview?.centerYAnchor {
            centerYAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        }
    }
    
    public func anchorCenterSuperview() {
        anchorCenterXToSuperview()
        anchorCenterYToSuperview()
    }
}


extension UIAlertController {
    
    func show(viewController: UIViewController? = nil) {
        
        let topViewController = viewController ?? UIApplication.shared.delegate?.window?!.rootViewController
        
        topViewController?.present(self, animated: true, completion: nil)
    }
    
    static func showAlert(title: String, message: String, cancelButtonTitle: String? = nil, confirmationButtonTitle: String? = nil, viewController: UIViewController? = nil, dissmissBlock: (()-> Void)? = nil , cancelBlock: (()-> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        // Cancel Button
        if cancelButtonTitle != nil {
            alert.addAction(UIAlertAction(title: cancelButtonTitle, style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                cancelBlock?()
            }))
        }
        // Confirmation button
        if confirmationButtonTitle != nil {
            alert.addAction(UIAlertAction(title: confirmationButtonTitle, style: UIAlertActionStyle.default, handler: { (action) -> Void in
                dissmissBlock?()
            }))
        }
        
        // Show
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            alert.show(viewController: viewController)
        })
    }
}

// MARK: - Navigation Bar Configuration


extension String {
    func deleteHTMLTag(tag:String) -> String {
        return self.replacingOccurrences(of: "(?i)</?\(tag)\\b[^<]*>", with: "", options: .regularExpression, range: nil)
    }
    
    func deleteHTMLTags(tags:[String]) -> String {
        var mutableString = self
        for tag in tags {
            mutableString = mutableString.deleteHTMLTag(tag: tag)
        }
        return mutableString
    }
}


extension UIViewController {
    
    func addLeftBarButtonItem(_ image: UIImage?, target: Any, action: Selector) {
        
        let barButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: target, action: action)
        navigationItem.setLeftBarButton(barButtonItem, animated: true)
        navigationItem.leftBarButtonItem?.isEnabled = true
    }
    func buttonImageForEmptyState() -> UIImage {
        let capInsets = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7)
        
        let buttonWidth: CGFloat = Device.isPad() ? 400 : 300
        let padding: CGFloat = -((Device.currentDeviceWidth - buttonWidth)/2)
        let rectInsets = UIEdgeInsets(top: -19, left: padding, bottom: -19, right: padding)
        return UIImage(named: "borderButton")!.resizableImage(withCapInsets: capInsets, resizingMode: UIImageResizingMode.stretch).withAlignmentRectInsets(rectInsets)
    }
    
    func addRightBarButtonItem(_ image: UIImage?, target: Any, action: Selector) {
        
        let barButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: target, action: action)
        navigationItem.setRightBarButton(barButtonItem, animated: true)
        navigationItem.rightBarButtonItem?.isEnabled = true
    }
    func totalCarrinho( completion: @escaping (_ total: Int)->Void ) {
        var item = [Item]()
        guard Utils.hasConnection() else {
            
            guard let total = ItensManager.featchItem()?.count else {
                completion(0)
                return
            }
            completion(total)
            return
        }
        
        APIClient.shared.featchCarrinho(vendedor: "sumetal") { (itens, error) in
            
            var count = 0
            defer {
                completion(count)
            }
            
            if let _ = error {
                return
            }
            guard let  _itens = itens else{return}
            item = _itens
         
            count = _itens.count
        }
    }
    
    func carrinho(){
        let rightButton = UIButton(frame: CGRect(x:0,y:0,width:30,height:30))
        rightButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 22)
        rightButton.setTitle(String.fontAwesomeIcon(name: .shoppingBasket), for: .normal)
        rightButton.addTarget(self, action: #selector(UIViewController.carrinho(_:)), for: .touchUpInside)
        
        let rightButtonItem : UIBarButtonItem = UIBarButtonItem(customView: rightButton)
        
        let badgeView = M13BadgeView()
        badgeView.text = ""
        badgeView.textColor = UIColor.white
        badgeView.badgeBackgroundColor = UIColor.red
        badgeView.borderWidth = 1.0
        badgeView.borderColor = UIColor.white
        badgeView.horizontalAlignment = M13BadgeViewHorizontalAlignmentLeft
        badgeView.verticalAlignment = M13BadgeViewVerticalAlignmentTop
        badgeView.hidesWhenZero = true
        
        rightButton.addSubview(badgeView)
        
        totalCarrinho { (count) in
            badgeView.text = "\(count)"
        }
        
        self.navigationItem.rightBarButtonItem = rightButtonItem
    }
    
    func setupNavigationBarTitleWithImage(_ image: UIImage?, height: CGFloat = 30) {
        
        let titleView = UIImageView(frame:CGRect(x: 0, y: 0, width: 600, height: height))
        titleView.contentMode = .scaleAspectFit
        titleView.image = image
        
        navigationItem.titleView = titleView
    }
    
    func setupNavigationBarTitleWithImageURL(_ image: URL?, height: CGFloat = 30) {
        
        let titleView = UIImageView(frame:CGRect(x: 0, y: 0, width: 600, height: height))
        titleView.contentMode = .scaleAspectFit

        
        navigationItem.titleView = titleView
    }
    
    func setupNavigationBarTitle(_ title: String) {
        
        navigationItem.titleView = nil
        navigationItem.title = title
    }
    
    func setupNavigationBarBackButton() {
        navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(image: UIImage(named: "arrow_menu_close"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(UIViewController.back(_:)))
        navigationItem.leftBarButtonItem = newBackButton;
    }
    func carrinho(_ sender: UIBarButtonItem){
        myAppDelegate.sideMenuController?.rootViewController = UINavigationController(rootViewController: CarrinhoController.instantiateFromStoryboard())
    }
    func back(_ sender: UIBarButtonItem) {
        
     
        
        if presentingViewController == nil {
            navigationController?.popViewController(animated: true)
            return
        }
        
        dismiss(animated: true, completion: nil)
    }
}


// MARK: - UIBarButtonItem Helper

public extension UIBarButtonItem {
    
    public static func barButtonItemWith(image: UIImage?, target: AnyObject, action: Selector) -> UIBarButtonItem {
        return UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: target, action: action)
    }
    
    public static func barButtonItemWith(image: UIImage?, title: String?, titleFont: UIFont = UIFont.systemFont(ofSize: UIFont.systemFontSize), titleColor: UIColor = UIColor.white, target: AnyObject, action: Selector) -> UIBarButtonItem {
        
        let customButton = UIButton(type: .custom)
        
        var imageSize = CGSize(width: 32.0, height: 32.0)
        if let size = image?.size {
            imageSize.width = min(imageSize.width, size.width)
            imageSize.height = min(imageSize.height, size.height)
        }
        customButton.frame.size = imageSize
        customButton.addTarget(target, action: action, for: .touchUpInside)
        
        // Image
        if let _image = image {
            customButton.setImage(_image, for: UIControlState())
        }
        
        // Title
        if let _title = title, _title.characters.count > 0 {
            customButton.setTitle("  \(_title)", for: UIControlState())
            customButton.titleLabel?.font =  titleFont
            customButton.titleLabel?.text = customButton.titleLabel?.text?.uppercased()
            customButton.titleLabel?.textColor = titleColor
            customButton.frame.size.width = 100
        }
        
        customButton.contentHorizontalAlignment =  UIControlContentHorizontalAlignment.left
        
        return UIBarButtonItem(customView: customButton)
    }
    
}
// MARK: - Reusable View

protocol ReusableView: class {
    static var defaultReuseIdentifier: String { get }
}

extension ReusableView where Self: UIView {
    static var defaultReuseIdentifier: String {
        return NSStringFromClass(self).components(separatedBy: ".").last ?? String(describing: Mirror(reflecting: self).subjectType)
    }
}
extension ProdutoCell: ReusableView{}
extension ClientesCell: ReusableView{}


// MARK: - String(Hash)

//extension String {
//    var hashString: String { return self.q_sha512() }
//}

extension NumberFormatter {
    convenience init(style: Style) {
        self.init()
        numberStyle = style
    }
}

extension Formatter {
    static let currency = NumberFormatter(style: .currency)
    static let currencyUS: NumberFormatter = {
        let formatter = NumberFormatter(style: .currency)
        formatter.locale = Locale(identifier: "en_US")
        return formatter
    }()
    static let currencyBR: NumberFormatter = {
        let formatter = NumberFormatter(style: .currency)
        formatter.locale = Locale(identifier: "pt_BR")
        return formatter
    }()
}

extension String {
    var floatValues: Float {
        return (self as NSString).floatValue
    }

}
extension FloatingPoint {
    var currency: String {
        return Formatter.currency.string(for: self) ?? ""
    }
    var currencyUS: String {
        return Formatter.currencyUS.string(for: self) ?? ""
    }
    var currencyBR: String {
        return Formatter.currencyBR.string(for: self) ?? ""
    }
    
}
