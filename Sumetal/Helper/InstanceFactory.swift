//
//  InstanceFactory.swift
//  Sumetal
//
//  Created by Thiago Santos on 05/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

struct InstanceFactory {
    
    static let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    static let navigationController = InstanceFactory.storyboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController
    
    static let login = LoginController.instantiateFromStoryboard()
    static let home =  UINavigationController(rootViewController: HomeCatController.instantiateFromStoryboard())
     static let selectionCliente =  UINavigationController(rootViewController: SelectionClienteTableController.instantiateFromStoryboard())
       static let cadastroCliente =  UINavigationController(rootViewController: CadastroLojaController.instantiateFromStoryboard())
    static let pedido =  UINavigationController(rootViewController: PedidosController.instantiateFromStoryboard())
     static let carrinho =  UINavigationController(rootViewController: CarrinhoController.instantiateFromStoryboard())
}
