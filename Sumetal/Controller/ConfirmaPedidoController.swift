//
//  ConfirmaPedidoController.swift
//  Sumetal
//
//  Created by Thiago Santos on 01/08/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class ConfirmaPedidoController: UIViewController {
    var pedido: Pedido?
    
    @IBOutlet weak var clienteLabel: UILabel!
    
    @IBOutlet weak var valorBrutoLabel: UILabel!
    
    @IBOutlet weak var descontoLabel: UILabel!

   
    @IBOutlet weak var valorLiquidoLabel: UILabel!
    
    
    @IBOutlet weak var datapedidoLabel: UILabel!
    
    
    @IBOutlet weak var formaLabel: UILabel!
    @IBOutlet weak var condicaoLabel: UILabel!
    
    @IBOutlet weak var obsLabel: UILabel!
    
    
    
    
    
    @IBAction func sendPedido(_ sender: Any) {
        self.navigationController?.view.showActivityView()
        
      
        
        guard let _pedido = self.pedido else{ return }
        
        APIClient.shared.sendPedidos(_pedido) { (status, error) in
            defer {
                self.navigationController?.view.hideActivityView()
            }
            guard status else {
                UIAlertController.showAlert(title: "Opss Error !!!", message: "Erro ao enviar pedido", cancelButtonTitle: "OK") {
                }
                            return
            }
            UIAlertController.showAlert(title: "Pedido enviado !!!", message: "Pedido efetuado", cancelButtonTitle: "OK") {
                myAppDelegate.sideMenuController?.rootViewController =  InstanceFactory.home
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarTitleWithImage(#imageLiteral(resourceName: "nav_sumetal"))
        setupNavigationBarBackButton()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let _pedido = pedido else{ return }
        clienteLabel.text = _pedido.nomeCliente
        valorBrutoLabel.text = "Valor Bruto: \(_pedido.valorBruto)"
        valorLiquidoLabel.text = "Valor Liquido: \(_pedido.valorBruto)"
        descontoLabel.text = "Desconto: \(_pedido.valorDesconto)"
        datapedidoLabel.text = "Data pedido: \(_pedido.dataPedido)"
        formaLabel.text = "Forma de Pagamento: \(_pedido.condicoesPagamento)"
        condicaoLabel.text = "Condiçoes de Pagamento: \(_pedido.condicoes)"
        obsLabel.text = "Obs: \(_pedido.obs)"
    }
   
    // MARK: - Instance
    class func instantiateFromStoryboard() -> ConfirmaPedidoController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "ConfirmaPedidoController") as! ConfirmaPedidoController
    }
  


}
