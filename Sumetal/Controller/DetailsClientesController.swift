//
//  DetailsClientesController.swift
//  Sumetal
//
//  Created by Thiago Santos on 16/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class DetailsClientesController: UIViewController {
    var user: User?
    
    
    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var enderecoLabel: UILabel!
    @IBOutlet weak var cidadeLabel: UILabel!
    @IBOutlet weak var estadoLabel: UILabel!
    @IBOutlet weak var cepLabel: UILabel!
    @IBOutlet weak var telefoneLabel: UILabel!
    @IBOutlet weak var cnpjLabel: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarBackButton()
        setupNavigationBarTitleWithImage(#imageLiteral(resourceName: "nav_sumetal"))
        addRightBarButtonItem(#imageLiteral(resourceName: "carrinho"), target: self, action: #selector(carrinho(_:)))
        
        guard let _user = user else { return }
        
        self.nomeLabel.text          = "Nome: \(_user.nome)"
        self.enderecoLabel.text      = "Endereço: \(_user.endereco)"
        self.cidadeLabel.text        = "Cidade: \(_user.cidade)"
        self.estadoLabel.text        = "Estado: \(_user.estado)"
        self.cepLabel.text           = "Cep: \(_user.cep)"
        self.telefoneLabel.text      = "Telefone: \(_user.telefone)"
        self.cnpjLabel.text          = "CNPJ: \(_user.cnpj)"
       
    }

 
    // MARK: - Instance
    class func instantiateFromStoryboard() -> DetailsClientesController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "DetailsClientesController") as! DetailsClientesController
    }

}
