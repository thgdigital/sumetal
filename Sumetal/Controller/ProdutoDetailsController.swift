//
//  ProdutoDetailsController.swift
//  Sumetal
//
//  Created by Thiago Santos on 13/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class ProdutoDetailsController: UITableViewController {
    var produto: Produto?
    var qtD = 1
     let cdManager = CoreDataManager.sharedInstance
    @IBOutlet weak var qtdSepper: UILabel!
    
    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var titlelabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var precoLabel: UILabel!
    @IBOutlet weak var tamanhoLabel: UILabel!
    @IBOutlet weak var dimensacaoLabel: UILabel!
    @IBOutlet weak var pesoLabel: UILabel!
    
    @IBOutlet weak var correiasLabel: UILabel!
    
    @IBOutlet weak var formatosLabel: UILabel!
    
    @IBOutlet weak var acabamentosLabel: UILabel!
    
    @IBOutlet weak var fundoLabel: UILabel!
    @IBOutlet weak var fabricacaoLabel: UILabel!
    @IBOutlet weak var fixacaoLabel: UILabel!
    @IBOutlet weak var modalidadeLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarBackButton()
        setupNavigationBarTitleWithImage(#imageLiteral(resourceName: "nav_sumetal"))
         carrinho()
        self.tableView.tableFooterView = UIView()
        self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 1))
        
    
    }
    
    
    func setupLayout(_produto: Produto){
        self.produto  = _produto
        self.bannerImageView.sd_setImage(with: URL(string: _produto.thumb), placeholderImage: #imageLiteral(resourceName: "logo-place"))
        self.titlelabel.text = _produto.nome
        self.subTitleLabel.text = "Referencia: \(_produto.codigo)"
        self.precoLabel.text = "Preços: R$ \(_produto.preco)"
        self.tamanhoLabel.text = "Tamanho: \(_produto.tamanho)"
        self.dimensacaoLabel.text = "Dimensão: \(_produto.dimensao)"
        self.pesoLabel.text = "Peso: \(_produto.peso)"
        self.correiasLabel.text = "Correiras: \(_produto.correia)"
        self.formatosLabel.text = "Formatos: \(_produto.formato)"
        self.acabamentosLabel.text = "Acabamentos \(_produto.acabamento)"
        self.fundoLabel.text    = "Fundo: \(_produto.fundo)"
        self.fabricacaoLabel.text = "Fabricação: \(_produto.fabricacao)"
        self.fixacaoLabel.text  = "Fixação: \(_produto.fixacao)"
        self.modalidadeLabel.text = "Modalidade: \(_produto.modalidade)"
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.view.showActivityView()
        if !Utils.hasConnection(){
            defer{
                self.navigationController?.view.hideActivityView()
            }
            // Create a sort descriptor object that sorts on the "title"
            // property of the Core Data object
            let sortDescriptor = NSSortDescriptor(key: "nome", ascending: true)
            guard let _produtos = produto else { return }
            
            // Create a new predicate that filters out any object that
            // doesn't have a title of "Best Language" exactly.
            let predicate = NSPredicate(format: "nome == %@", _produtos.nome)
            
            let cdManager = CoreDataManager.sharedInstance
            guard  let _produto =  cdManager.customSearch("ProdutoManager", predicate: predicate, sortDescriptor: sortDescriptor) as? [ProdutoManager] else {
                
                return
            }
            let  _prod  = _produto.map( {return Produto(core: $0)}).first
            self.produto = _prod
            self.setupLayout(_produto: _prod!)
            self.tableView.reloadData()
            
            return
        }
        
        
        
        
        if let prod = produto {
            APIClient.shared.produtoID(id: prod.id) { (produ, error) in
                defer{
                    self.navigationController?.view.hideActivityView()
                }
                if  let _error = error as NSError? {
                    print(_error.code)
                    return
                }
                
                guard let _produto = produ else{ return }
                self.setupLayout(_produto: _produto)
                
            }
        }
    }
    
    @IBAction func stepperSend(_ sender: UIStepper) {
        self.qtD  = Int(sender.value)
        self.qtdSepper.text = "QTD \(self.qtD)"
    }
    
    @IBAction func sendCarrinho(_ sender: Any) {
        self.navigationController?.view.showActivityView()
        var item = Item()
        
           if !Utils.hasConnection(){
        if let _produto = produto {
          
            
            item.nome = _produto.nome
            item.imagem = _produto.thumb
            item.qtd = "\(self.qtD)"
            item.ref = _produto.codigo
            item.valor = _produto.preco
            item.produto = _produto.id
            
            let itens = self.cdManager.new("Itens") as! Itens
            
                itens.imagem = item.imagem
                itens.nome = item.nome
                itens.qtd    = Int16(self.qtD)
                itens.ref    = item.ref
                itens.valor  = item.valor
                cdManager.saveObj(itens)
          
           
                myAppDelegate.sideMenuController?.rootViewController = InstanceFactory.carrinho

        
            }
            return
        }
         guard let _produto = produto else{
            return
        }
    
            item.nome = _produto.nome
            item.imagem = _produto.thumb
            item.produto = _produto.id
            item.ref    = _produto.codigo
            item.valor  = _produto.preco
            item.qtd    = "\(self.qtD)"
            
            
            
            APIClient.shared.sendItens(item, completionHandler: { (status, error) in
                defer{
                    self.navigationController?.view.hideActivityView()
                }
                if let _error = error as NSError?{
                    print(_error)
                    return
                }
                if  status {
                 
                    UIAlertController.showAlert(title: "Item Adicionado", message: "Dados adicionado com sucesso", cancelButtonTitle: "Ok", cancelBlock: {
                        myAppDelegate.sideMenuController?.rootViewController = InstanceFactory.carrinho
                    })
                    return
                }
                UIAlertController.showAlert(title: "Opss Error :(", message: "Erro ao adiconar item no carrinho", cancelButtonTitle: "Ok")
                
            })
        
        
        
    }
 
    // MARK: - Instance
    class func instantiateFromStoryboard() -> ProdutoDetailsController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "ProdutoDetailsController") as! ProdutoDetailsController
    }

   

}
