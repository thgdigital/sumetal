//
//  SelectionClienteTableController.swift
//  Sumetal
//
//  Created by Thiago Santos on 22/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

protocol SelecionCliente {
    func error(_ error: Error)
    func didSelection(_ user: User)
    func close()
    func pushCadastro()
}

class SelectionClienteTableController: UITableViewController {
    var users = [User]()
    var selecione: SelecionCliente?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        featch()
       
        setupNavigationBarTitleWithImage(#imageLiteral(resourceName: "nav_sumetal"))
        self.tableView.tableFooterView = UIView()
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
       
        let ButtonItem: UIBarButtonItem = {
           let  bt = UIBarButtonItem()
            bt.title = "Fechar"
            bt.target = self
            bt.action = #selector(close)
            return bt
        }()
        
        
        self.navigationItem.rightBarButtonItem = ButtonItem
    }
    
    
    @IBAction func cadastroClinte(_ sender: Any) {
      
        dismiss(animated: true, completion: {
              self.selecione?.pushCadastro()
        })
        
    }
    
    
    
    func close(){
        self.selecione?.close()
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Instance
    class func instantiateFromStoryboard() -> SelectionClienteTableController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "SelectionClienteTableController") as! SelectionClienteTableController
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return users.count
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selecione?.didSelection(users[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
    func featch(){
        self.navigationController?.view.showActivityView()
        if !Utils.hasConnection(){
            defer{
                self.navigationController?.view.hideActivityView()
            }
            if let _users = ClientesManager.featchUser(){
                self.users = _users
                self.tableView.reloadData()
            }
            
            return
        }
     APIClient.shared.fetchClientes(user: "sumetal") { (clientes, error) in
        defer{
            self.navigationController?.view.hideActivityView()
        }
        if let _erro = error as NSError?{
            print(_erro.code)
            return
            }
        guard let _clientes = clientes else { return }
            self.users = _clientes
              self.tableView.reloadData()
        }
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! SelectionClientesCell
        cell.clientes = users[indexPath.row]
        return cell
    }
}
extension SelectionClienteTableController: DZNEmptyDataSetDelegate, DZNEmptyDataSetSource{
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "logo-place")
    }
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let atrributed = NSAttributedString(string:"Opss !!!", attributes:
            [NSForegroundColorAttributeName: UIColor.lightGray])
        return atrributed
        
    }
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let atrributed = NSAttributedString(string:"Não existem clientes", attributes:
            [NSForegroundColorAttributeName: UIColor.lightGray])
        return atrributed
    }
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        let atrributed = NSAttributedString(string:"Carregar Novamente", attributes:
            [NSForegroundColorAttributeName: UIColor.white])
        return atrributed
    }
    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> UIImage! {
        return buttonImageForEmptyState()
    }
    func backgroundColor(forEmptyDataSet scrollView: UIScrollView!) -> UIColor! {
        return .clear
    }
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        self.featch()
    }

}
