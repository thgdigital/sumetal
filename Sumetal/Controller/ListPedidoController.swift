//
//  ListPedidoController.swift
//  Sumetal
//
//  Created by Thiago Santos on 24/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class ListPedidoController: BaseTableViewController {
    var pedidos: [Pedido] = []
    override func viewDidLoad() {
        super.viewDidLoad()
       self.featchPedidos()
    }


    func featchPedidos() {
        self.navigationController?.view.showActivityView()
        APIClient.shared.feacthPedidos("sumetal") { (pedidos, error) in
            defer{
                self.navigationController?.view.hideActivityView()
            }
            if let _error = error as NSError?{
                print(_error)
                return
            }
            guard let _pedidos = pedidos else {return}
            self.pedidos = _pedidos
            self.tableView.reloadData()
        }
    }


    // MARK: - Table view data source


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.pedidos.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as!ListPedidoCell
        cell.pedido = pedidos[indexPath.row]


        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller  = DetailsController.instantiateFromStoryboard()
            controller.pedido = pedidos[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
    
    }
    // MARK: - Instance
    class func instantiateFromStoryboard() -> ListPedidoController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "ListPedidoController") as! ListPedidoController
    }

   
}
