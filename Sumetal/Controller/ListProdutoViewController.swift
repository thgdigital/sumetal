//
//  ListProdutoViewCell.swift
//  Sumetal
//
//  Created by Thiago Santos on 11/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class ListProdutoViewController: UITableViewController {
    var produtos: [Produto] = []
    var categoria: String?
    var subCategoria: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarTitleWithImage(#imageLiteral(resourceName: "nav_sumetal"))
 carrinho()
        setupNavigationBarBackButton()
        self.tableView.tableFooterView = UIView()
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        
        
     
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidLoad()
         self.featch()
         
    }
    func featch(){

        guard let cat = categoria, let subCat = subCategoria else{ return }
        
        if !Utils.hasConnection(){
            
            // Create a sort descriptor object that sorts on the "title"
            // property of the Core Data object
            let sortDescriptor = NSSortDescriptor(key: "nome", ascending: true)
            
            
            // Create a new predicate that filters out any object that
            // doesn't have a title of "Best Language" exactly.
            let predicate = NSPredicate(format: "categoria == %@ AND subcategoria == %@", cat.capitalized, subCat.capitalized)
           
            let cdManager = CoreDataManager.sharedInstance
            guard  let _produto =  cdManager.customSearch("ProdutoManager", predicate: predicate, sortDescriptor: sortDescriptor) as? [ProdutoManager] else {
                
                return
            }
            
            for _prod  in _produto{
                var produto = Produto()
             
                produto.acabamento  = _prod.acabamento!
                produto.categoria   = _prod.categoria!
                produto.codigo      = _prod.codigo!
                produto.correia     = _prod.correia!
                produto.dimensao    = _prod.dimensao!
                produto.fabricacao  = _prod.fabricacao!
                produto.fixacao     = _prod.fixacao!
                produto.formato     = _prod.formato!
                produto.fundo       = _prod.fundo!
                produto.id          = Int(_prod.id)
                produto.modalidade  = _prod.modalidade!
                produto.nome        = _prod.nome!
                produto.peso        = _prod.peso!
                produto.tamanho     = _prod.tamanho!
                produto.thumb       = _prod.thumb!
                produtos.append(produto)
                    
                
                
            }
            tableView.reloadData()
            
            return
        }
        
         self.navigationController?.view.showActivityView()
        APIClient.shared.produtofilter(categoria: cat, subCategoria: subCat) { (produtos, error) in
            defer{
                self.navigationController?.view.hideActivityView()
            }
            if let _error = error {
                print(_error)
                return
            }
            guard let produ = produtos else { return }
            
            guard produ.count > 0 else{
              
                
                return
            }
            for produto in produ{
                self.produtos.append(produto)
            }
            self.tableView.reloadData()
            
        }
    }
   
 
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return produtos.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "teste", for: indexPath) as! ProdutoCell
        cell.produto = produtos[indexPath.row]
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller  = ProdutoDetailsController.instantiateFromStoryboard()
            controller.produto = produtos[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: - Instance
    class func instantiateFromStoryboard() -> ListProdutoViewController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "ListProdutoViewController") as! ListProdutoViewController
    }
     override func buttonImageForEmptyState() -> UIImage {
        let capInsets = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7)
        
        let buttonWidth: CGFloat = Device.isPad() ? 400 : 300
        let padding: CGFloat = -((Device.currentDeviceWidth - buttonWidth)/2)
        let rectInsets = UIEdgeInsets(top: -19, left: padding, bottom: -19, right: padding)
        return UIImage(named: "borderButton")!.resizableImage(withCapInsets: capInsets, resizingMode: UIImageResizingMode.stretch).withAlignmentRectInsets(rectInsets)
    }
}
extension ListProdutoViewController : DZNEmptyDataSetDelegate, DZNEmptyDataSetSource{
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "logo-place")
    }
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let atrributed = NSAttributedString(string:"Opss Erro !!!", attributes:
            [NSForegroundColorAttributeName: UIColor.lightGray,
             NSFontAttributeName: UIFont.boldSystemFont(ofSize: 24)
            ])
        return atrributed

    }
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let atrributed = NSAttributedString(string:"Não foi possivel carregar dados.", attributes:
            [NSForegroundColorAttributeName: UIColor.lightGray,
             NSFontAttributeName: UIFont.systemFont(ofSize: 20)
             ])
        return atrributed
    }
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        let atrributed = NSAttributedString(string:"Tentar Novamente", attributes:
            [NSForegroundColorAttributeName: UIColor.white])
        return atrributed
    }
    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> UIImage! {
        return buttonImageForEmptyState()
    }
    func backgroundColor(forEmptyDataSet scrollView: UIScrollView!) -> UIColor! {
        return .clear
    }
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        self.featch()
    }
    
    
}
