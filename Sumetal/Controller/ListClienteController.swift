//
//  ListClienteController.swift
//  Sumetal
//
//  Created by Thiago Santos on 14/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class ListClienteController: BaseTableViewController, UINavigationControllerDelegate {
    var users = [User]()
    override func viewDidLoad() {
        
        super.viewDidLoad()
          self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 1))
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        self.featchClientes()
      
    }
   
    func featchClientes(){
         self.navigationController?.view.showActivityView()
        if !Utils.hasConnection(){
            defer{
                self.navigationController?.view.hideActivityView()
            }
            if let _users = ClientesManager.featchUser(){
                self.users = _users
                 self.tableView.reloadData()
            }
         
            return
        
        }
        
        APIClient.shared.fetchClientes(user: "sumetal") { (users, error) in
            defer{
                self.navigationController?.view.hideActivityView()
            }
            if let _error = error{
                    print(_error)
                return
            }
            guard  let _users = users else{ return }
            self.users = []
            for _user  in _users{
                self.users.append(_user)
            }
            self.tableView.reloadData()
        }
    }
   
    // MARK: - Instance
    class func instantiateFromStoryboard() -> ListClienteController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "ListClienteController") as! ListClienteController
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath)  as! ClientesCell
        cell.clientes = users[indexPath.row]
        return cell
    }

  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller  = DetailsClientesController.instantiateFromStoryboard()
            controller.user = users[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)

    }
    
}
extension ListClienteController : DZNEmptyDataSetDelegate, DZNEmptyDataSetSource{
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "logo-place")
    }
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let atrributed = NSAttributedString(string:"Opss Erro !!!", attributes:
            [NSForegroundColorAttributeName: UIColor.lightGray])
        return atrributed
        
    }
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let atrributed = NSAttributedString(string:"Não foi possivel carregar dados.", attributes:
            [NSForegroundColorAttributeName: UIColor.lightGray,
             NSFontAttributeName: UIFont.boldSystemFont(ofSize: 24)])
        return atrributed
    }
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        let atrributed = NSAttributedString(string:"Tentar Novamente", attributes:
            [NSForegroundColorAttributeName: UIColor.white])
        return atrributed
    }
    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> UIImage! {
        return buttonImageForEmptyState()
    }
    func backgroundColor(forEmptyDataSet scrollView: UIScrollView!) -> UIColor! {
        return .clear
    }
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        self.featchClientes()
    }
    
    
}
