//
//  CadastroLojaController.swift
//  Sumetal
//
//  Created by Thiago Santos on 22/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class CadastroLojaController: BaseTableViewController {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var cnpjTextField: UITextField!
    @IBOutlet weak var senhaTextField: UITextField!
    @IBOutlet weak var endercoTextField: UITextField!
    @IBOutlet weak var cidadeTextField: UITextField!
    @IBOutlet weak var cepTextField: UITextField!
    @IBOutlet weak var telefoneTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    // MARK: - Instance
    class func instantiateFromStoryboard() -> CadastroLojaController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "CadastroLojaController") as! CadastroLojaController
    }


}
