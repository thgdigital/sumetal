//
//  CarrinhoController.swift
//  Sumetal
//
//  Created by Thiago Santos on 18/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import DZNEmptyDataSet


class CarrinhoController:BaseTableViewController {
    var itens = [Item]()
    var boxfinalizar:  BoxFinalizarView!
    var pedido = Pedido()
    let formate: DateFormatter = {
        let formato = DateFormatter()
        formato.dateFormat = "dd/MM/yyyy"
        return formato
    }()
    @IBOutlet weak var colletionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.tableView.emptyDataSetDelegate = self
        self.tableView.emptyDataSetSource = self
        self.tableView.allowsSelection = true
        
        let nib = UINib(nibName: "ItemPedidoCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "itemId")
        let nibForm = UINib(nibName: "FormPedidoCell", bundle: nil)
        self.tableView.register(nibForm, forCellReuseIdentifier: "formId")
        boxfinalizar = BoxFinalizarView.instanciateFromNib()
        boxfinalizar.delegate = self

       self.colletionView.delegate = self
        self.colletionView.dataSource = self
        self.colletionView.register(UINib(nibName:"CarrinhoItemViewCell", bundle: nil), forCellWithReuseIdentifier: "cellId")
      
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchCarrinho()
        tableView.estimatedRowHeight = 400
        tableView.rowHeight = UITableViewAutomaticDimension
        
        NotificationCenter.default.addObserver(self, selector: #selector(totalNotification(_:)), name: NSNotification.Name(rawValue: "totalNotification"), object: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func totalNotification(_ notification: Foundation.Notification){
        carrinho()
    }
    func fetchCarrinho() {
        defer{
            self.navigationController?.view.hideActivityView()
        }
            self.navigationController?.view.showActivityView()
        if !Utils.hasConnection(){
            guard let _pedidos = ItensManager.featchItem() else {return}
            self.itens = _pedidos
            self.colletionView.reloadData()
            self.tableView.reloadData()
            return
        }
        
       
        APIClient.shared.featchCarrinho(vendedor: "sumetal") { (itens, error) in
            defer{
                self.navigationController?.view.hideActivityView()
            }
            if let _error = error {
                print(_error)
                return
            }
            guard let  _itens = itens else{return}
            self.itens = _itens
            self.colletionView.reloadData()
            self.tableView.reloadData()
            
            
        }
    }
   
 


    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if itens.count > 0{
//
            return 1
        }
        return 0
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "formId", for: indexPath) as! FormPedidoCell
        cell.delegate = self
            tableView.separatorStyle = .none
        
        if !Utils.hasConnection(){
            cell.setupLayout()
        }else{
        cell.item = itens[indexPath.row]
            
        }
       return cell

        
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return 480
    }
    // MARK: - Instance
    class func instantiateFromStoryboard() -> CarrinhoController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "CarrinhoController") as! CarrinhoController
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            
            let codigo = itens[indexPath.row].produto
           
            self.navigationController?.view.showActivityView()
        APIClient.shared.deleteCarrinho(codigo: codigo, completionHandler: { (status) in
            
            defer{
                self.navigationController?.view.hideActivityView()
            }
            if status{
             
                self.itens.remove(at: indexPath.row)
                 tableView.reloadData()
             
                
                self.fetchCarrinho()
                return
            }
            
            
        })
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
 



}
extension CarrinhoController : DZNEmptyDataSetDelegate, DZNEmptyDataSetSource{
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "carrinho-empty")
    }
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let atrributed = NSAttributedString(string:"Tá vazio!", attributes:
            [NSForegroundColorAttributeName: UIColor.lightGray])
        return atrributed
        
    }
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let atrributed = NSAttributedString(string:"Ainda não há nenhum item no seu carrinho.", attributes:
            [NSForegroundColorAttributeName: UIColor.lightGray])
        return atrributed
    }
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        let atrributed = NSAttributedString(string:"Continuar comprando", attributes:
            [NSForegroundColorAttributeName: UIColor.white])
        return atrributed
    }
    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> UIImage! {
        return buttonImageForEmptyState()
    }
    func backgroundColor(forEmptyDataSet scrollView: UIScrollView!) -> UIColor! {
        return .clear
    }
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        myAppDelegate.sideMenuController?.rootViewController = UINavigationController(rootViewController: HomeCatController.instantiateFromStoryboard())
       
    }
    
    
}
extension CarrinhoController : BoxFinalizarViewDelegate {
    func prevCompra(){
       myAppDelegate.sideMenuController?.rootViewController = InstanceFactory.home
        
    }
    func nextCompra(pedido: Pedido){
        self.pedido = pedido
        
        let viewController = SelectionClienteTableController.instantiateFromStoryboard()
        viewController.selecione = self
        
        self.navigationController?.present(UINavigationController(rootViewController: viewController), animated: true, completion: nil)
       
      
    }
}

// MARK: - Collection view data source

extension CarrinhoController :UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, CarrinhoDelegate{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: 120)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"cellId", for: indexPath)  as! CarrinhoItemViewCell
        cell.item =  itens[indexPath.row]
        cell.delegate = self
        if itens.count == (indexPath.row + 1){
            cell.separador.isHidden = true
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return itens.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func remover(_ index: Int) {
        self.navigationController?.view.showActivityView()
        
        
        if !Utils.hasConnection(){
        guard let itens  = ItensManager.featchItens() else {return}
        
        defer{
            self.navigationController?.view.hideActivityView()
        }
        
        guard let item = itens.filter({ return $0.produto == Int16(index)}).first else{return}
        
        _ = ItensManager.delete(item)
        self.fetchCarrinho()
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: "totalNotification"), object: nil)
            return
        }
        APIClient.shared.deleteCarrinho(codigo: index, completionHandler: { (status) in
            
            defer{
                self.navigationController?.view.hideActivityView()
            }
            if status{
                self.fetchCarrinho()
                return
            }
            
            
        })
    }
  
}
extension CarrinhoController : SelecionCliente {
    
    func error(_ error: Error){
        
    }
    func didSelection(_ user: User){
        let date = Date()
        
    
        pedido.nomeCliente = user.nome
        pedido.dataPedido = self.formate.string(from: date)
        pedido.itens = self.itens
        if !Utils.hasConnection(){
            pedido.valorBruto = "\(ItensManager.valorTotal())"
        }else{
            if let total = itens.first?.total{
                pedido.valorBruto = "\(String(describing: total))"
            }
        }
        
        for  _item in self.itens{
            ItensManager.delete(_item)
        }
//      
        let viewController  = ConfirmaPedidoController.instantiateFromStoryboard()
        viewController.pedido = pedido

        self.navigationController?.pushViewController(viewController, animated: true)
     
    }
    func close(){
        
        
    }
    func pushCadastro() {
     
        
    
        let view = CadastroLojaController.instantiateFromStoryboard()
        myAppDelegate.sideMenuController?.rootViewController = UINavigationController(rootViewController: view)
    }
}
