//
//  LoginController.swift
//  Sumetal
//
//  Created by Thiago Santos on 05/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class LoginController: BaseViewController {

    @IBOutlet weak var senhaTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarBackButton()
        emailTextField.delegate = self
        senhaTextField.delegate = self
        self.navigationItem.title = "LOGIN"
    }
    
    @IBAction func sendLogin(_ sender: Any) {
        activityIndicatorView.startAnimating()
        guard let username = emailTextField.text , let senha = senhaTextField.text , username != "" , senha != "" else{
            UIAlertController.showAlert(title: "Opss Error", message: "Verificar se exitem dados em brancos ou e-mail invalido.", cancelButtonTitle: "Fechar",  cancelBlock: nil)
            activityIndicatorView.stopAnimating()
            return
         
        }
        APIClient.shared.login(user: username, pass: senha, completionHandler: { (user, error) in
            defer{
                self.activityIndicatorView.stopAnimating()
            }
            guard let  _error = error else{
                if let usr = user {
                    let urs = ["userName": usr.userName , "id": usr.id] as [String : Any]
                    let defaults = UserDefaults.standard
                    defaults.set(urs, forKey: "user")
                    defaults.synchronize()
                    self.setRootViewController()
                }
               
                
                
                
                return
                
            }
             UIAlertController.showAlert(title: "Opss Error", message: _error.localizedDescription, cancelButtonTitle: "Fechar",  cancelBlock: nil)
        })
    }
    
    // MARK: - Instance
    
    class func instantiateFromStoryboard() -> LoginController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "LoginController") as! LoginController
    }
    private func setRootViewController(){
        let sideMenu = myAppDelegate.rootView()
        myAppDelegate.sideMenuController = sideMenu
        myAppDelegate.window?.rootViewController = sideMenu
    }
}

