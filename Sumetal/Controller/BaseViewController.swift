//
//  BaseViewController.swift
//  Sumetal
//
//  Created by Thiago Santos on 05/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    let activityIndicatorView: UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        aiv.hidesWhenStopped = true
        aiv.color = .black
        aiv.stopAnimating()
        return aiv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BaseViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        view.addSubview(activityIndicatorView)
        activityIndicatorView.anchorCenterXToSuperview()
        activityIndicatorView.anchorCenterYToSuperview()
        setupNavigationBarTitleWithImage(#imageLiteral(resourceName: "nav_sumetal"))
    }

    func dismissKeyboard() {
        view.endEditing(true)
    }

}
extension BaseViewController : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        textField.resignFirstResponder()
        return true
    }
}
