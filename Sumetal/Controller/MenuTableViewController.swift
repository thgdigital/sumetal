//
//  MenuTableViewController.swift
//  Sumetal
//
//  Created by Thiago Santos on 06/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import LGSideMenuController

class MenuTableViewController: UITableViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorStyle = .none
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        myAppDelegate.sideMenuController?.hideLeftView(animated: true, delay: 0, completionHandler: {
            switch indexPath.row {
            case 0:
                let inicio = HomeCatController.instantiateFromStoryboard()
                self.setRootController(viewController: inicio)
            break
        case 1:
            let meuDados = DetailsUserController.instantiateFromStoryboard()
            self.setRootController(viewController: meuDados)
            
        case 2:
            let listCliente = ListClienteController.instantiateFromStoryboard()
            self.setRootController(viewController: listCliente)
        break
        case 3:
            let meusPedidos = ListPedidoController.instantiateFromStoryboard()
            self.setRootController(viewController: meusPedidos)
        break
        case 4:
            let listProduto = DatabaseSincronizerController.instantiateFromStoryboard()
            self.setRootController(viewController: listProduto)
        break
        case 6:
                
                let userdefaults = UserDefaults.standard
                userdefaults.removeObject(forKey: "user")
                userdefaults.synchronize()
                myAppDelegate.window?.rootViewController = UINavigationController(rootViewController: HomeController.instantiateFromStoryboard())
                break
        default:
            break
        }
    })

    }
    func setRootController(viewController: UIViewController){
        
        myAppDelegate.sideMenuController?.rootViewController = UINavigationController(rootViewController: viewController)
    }

    
    // MARK: - Instance
    
    class func instantiateFromStoryboard() -> MenuTableViewController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "MenuTableViewController") as! MenuTableViewController
    }
    
}
