//
//  DetailsUserController.swift
//  Sumetal
//
//  Created by Thiago Santos on 11/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class DetailsUserController: BaseTableViewController {

    var  user: User?
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var senhaTextField: UITextField!
    @IBOutlet weak var cnpjTextField: UITextField!
    @IBOutlet weak var enderecoTextField: UITextField!
    @IBOutlet weak var cidadeTextField: UITextField!
    @IBOutlet weak var cepTextField: UITextField!
    @IBOutlet weak var telefoneTextField: UITextField!
    @IBOutlet weak var lastCell: UITableViewCell!
    
    @IBOutlet weak var razaoSocialTextField: UITextField!
    @IBOutlet weak var nomeFantasiaTextField: UITextField!
    @IBOutlet weak var inscricaoEstadualTextField: UITextField!
    @IBOutlet weak var longadouroTextField: UITextField!
    @IBOutlet weak var estadoTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.setupDelegate()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        findProfile()
        self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 1))
      
    }
    
    
    @IBAction func sendUpdate(_ sender: Any) {
        guard let email = emailTextField.text, let username = userNameTextField.text, let cpnj = cnpjTextField.text, let senha = senhaTextField.text , let endereco = enderecoTextField.text, let cidade = cidadeTextField.text, let cep = cepTextField.text, let telefone = telefoneTextField.text ,let nome = nomeFantasiaTextField.text , let razao  = razaoSocialTextField.text,let inscricao =  inscricaoEstadualTextField.text, let longadoro = longadouroTextField.text,let estado = estadoTextField.text, email != "", username != "" , cpnj != "", senha != "", endereco != "" , cidade != "", cep != "", telefone != "" , razao != "" , nome != "" else{
            
            UIAlertController.showAlert(title: "Opss Error", message: "Verificar se exitem dados em brancos ou e-mail invalido.", cancelButtonTitle: "Fechar",  cancelBlock: {
                self.navigationController?.view.hideActivityView()
            })
            return
        }
        var usr = User()
        usr.email                               = email
        usr.userName                            = username
        usr.password                            = senha
        usr.cep                                 = cep
        usr.endereco                            = endereco
        usr.cidade                              = cidade
        usr.cnpj                                = cpnj
        usr.telefone                            = telefone
        usr.nome                                = nome
        usr.fantasia                            = razao
        usr.inscricao                           = inscricao
        usr.logradouro                          = longadoro
        usr.estado                              = estado
        APIClient.shared.updateUser(user: usr) { (status, error) in
            defer{
                self.navigationController?.view.hideActivityView()
            }
            if  error != nil{
                UIAlertController.showAlert(title: "Opss Error", message: "Error ao alterar dados", cancelButtonTitle: "Fechar",  cancelBlock: nil)
                return
            }
            UIAlertController.showAlert(title: "Dados Atualizado", message: "Dados atualizado com sucesso", cancelButtonTitle: "Fechar",  cancelBlock: nil)
        }
        
    }
    
    
    // MARK: - Instance
    class func instantiateFromStoryboard() -> DetailsUserController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "DetailsUserController") as! DetailsUserController
    }
    func findProfile(){
        self.navigationController?.view.showActivityView()
        APIClient.shared.fetchProfile(id: 6) { (user, error) in
            defer{
                self.navigationController?.view.hideActivityView()
            }
            if  error != nil{
              
                UIAlertController.showAlert(title: "Opss Erro", message: (error?.localizedDescription)!, cancelButtonTitle: "Fechar",  cancelBlock: {
                     myAppDelegate.sideMenuController?.rootViewController = UINavigationController(rootViewController: HomeCatController.instantiateFromStoryboard())
                })
                return
            }
            guard let usr = user else {
                UIAlertController.showAlert(title: "Opss Erro", message: "Usuário não exitem", cancelButtonTitle: "Fechar",  cancelBlock: nil)
                return
            }

            self.emailTextField.text                = usr.email
            self.userNameTextField.text             = usr.userName
            self.senhaTextField.text                = usr.password
            self.cepTextField.text                  = usr.cep
            self.enderecoTextField.text             = usr.endereco
            self.cidadeTextField.text               = usr.cidade
            self.cnpjTextField.text                 = usr.cnpj
            self.telefoneTextField.text             = usr.telefone
            self.razaoSocialTextField.text          = usr.nome
            self.nomeFantasiaTextField.text         = usr.fantasia
            self.inscricaoEstadualTextField.text    = usr.inscricao
            self.longadouroTextField.text           = usr.logradouro
            self.estadoTextField.text               = usr.estado
        }
        
    }
    private func setupDelegate(){
        self.emailTextField.delegate                  = self
        self.userNameTextField.delegate               = self
        self.senhaTextField.delegate                  = self
        self.cepTextField.delegate                    = self
        self.enderecoTextField.delegate               = self
        self.cidadeTextField.delegate                 = self
        self.cnpjTextField.delegate                   = self
        self.telefoneTextField.delegate               = self
        self.razaoSocialTextField.delegate            = self
        self.nomeFantasiaTextField.delegate           = self
        self.inscricaoEstadualTextField.delegate      = self
        self.longadouroTextField.delegate             = self
        self.estadoTextField.delegate                 = self
    }
}
extension DetailsUserController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        textField.resignFirstResponder()
        return true
    }
}
