//
//  HomeCatController.swift
//  Sumetal
//
//  Created by Thiago Santos on 07/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import RNActivityView
import DZNEmptyDataSet

class HomeCatController: BaseTableViewController {
    var categorias = ["sumetal","UFB","ABQM","Almir Cambra","Tião Carreiro","Motoclube","Personalizados"]

    override func viewDidLoad() {
        super.viewDidLoad()
        _ = App.getUser()
        tableView.emptyDataSetDelegate = self
        tableView.emptyDataSetSource = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
  
    }



 // MARK: - Instance
 
    class func instantiateFromStoryboard() -> HomeCatController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "HomeCatController") as! HomeCatController
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return categorias.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! CategoriaCell
        cell.nameTexField.text = categorias[indexPath.row].uppercased()
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller  = ListSubCategoriaController.instantiateFromStoryboard()
        controller.categoria = categorias[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}
extension HomeCatController: DZNEmptyDataSetDelegate, DZNEmptyDataSetSource{
    

}
