//
//  ViewController.swift
//  Sumetal
//
//  Created by Thiago Santos on 04/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class HomeController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    
    }
    
    @IBAction func pushLogin(_ sender: Any) {
        let login = LoginController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(login, animated: true)
    }
    @IBAction func pushCadastro(_ sender: Any) {
        let login = CadastroController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(login, animated: true)
    }
    
    // MARK: - Instance
    class func instantiateFromStoryboard() -> HomeController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "HomeController") as! HomeController
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
      
    }

}

