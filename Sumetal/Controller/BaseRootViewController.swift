//
//  RootViewController.swift
//  Sumetal
//
//  Created by Thiago Santos on 06/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import LGSideMenuController


class BaseRootViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        addLeftBarButtonItem(#imageLiteral(resourceName: "hamburguer"), target: self, action: #selector(toogleMenus(_:)))
        setupNavigationBarTitleWithImage(#imageLiteral(resourceName: "nav_sumetal"))
        carrinho()
        self.view.backgroundColor = .white
    }
     
    func toogleMenus(_ sender: UIBarButtonItem){
        myAppDelegate.sideMenuController?.toggleLeftViewAnimated()
    }
 


}
