//
//  ListSubCategoriaController.swift
//  Sumetal
//
//  Created by Thiago Santos on 12/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class ListSubCategoriaController: UITableViewController {
    var categoria : String?
    
    let subCat = ["fivelas","cintos","chaveiros","acessórios","troféus"]
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarTitleWithImage(#imageLiteral(resourceName: "nav_sumetal"))
         carrinho()
        setupNavigationBarBackButton()
        self.tableView.tableFooterView = UIView()

        let nib = UINib(nibName: "SubCatView", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "cellId")
    }
    // MARK: - Instance
    class func instantiateFromStoryboard() -> ListSubCategoriaController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "ListSubCategoriaController") as! ListSubCategoriaController
    }
   
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subCat.count
    }
  
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath)  as! SubcatCell
    
        
        cell.title = subCat[indexPath.row].uppercased()
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller  = ListProdutoViewController.instantiateFromStoryboard()
        controller.categoria = categoria
        controller.subCategoria = subCat[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
