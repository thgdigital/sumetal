//
//  DetailsController.swift
//  Sumetal
//
//  Created by Thiago Santos on 27/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class DetailsController: UITableViewController {
    
    var pedido : Pedido?
    var item: [Item] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "ItemDetailsPedidoCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "itemId")
        let nibForm = UINib(nibName: "DetailsPedidoCell", bundle: nil)
        self.tableView.register(nibForm, forCellReuseIdentifier: "cellId")
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 101
        setupNavigationBarBackButton()
        setupNavigationBarTitleWithImage(#imageLiteral(resourceName: "nav_sumetal"))
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        featchItem()
        
    }
    func featchItem(){
        guard let _pedido = pedido else{return}
        
        APIClient.shared.featchPedidoItem(id: _pedido.id) { (itens, error) in
            if let _error  = error as NSError?{
                print(_error.localizedDescription)
                return
            }
            guard let _itens = itens else{ return }
            self.item = _itens
            self.featchPedido()
            
        }
    }
    func featchPedido(){
        guard let _pedido = pedido else{return}
        APIClient.shared.featchPedidoDados(id: _pedido.id) { (pedido, error) in
            if let _error  = error as NSError?{
                print(_error.localizedDescription)
                return
            }
            guard let _pedido =  pedido else{return}
            self.pedido = _pedido
            self.tableView.reloadData()
        }
    }

    // MARK: - Instance
    class func instantiateFromStoryboard() -> DetailsController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "DetailsController") as! DetailsController
    }
    
 

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0{
            return item.count
        }
        return 1
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 120
        }
        return 260
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
           let cell = tableView.dequeueReusableCell(withIdentifier: "itemId", for: indexPath) as! ItemDetailsPedidoCell
            cell.item = item[indexPath.row]

            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! DetailsPedidoCell
            cell.pedido = pedido
            
            
            return cell
        }
        
        
    }

}
