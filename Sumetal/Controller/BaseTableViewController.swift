//
//  BaseTableViewController.swift
//  Sumetal
//
//  Created by Thiago Santos on 11/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class BaseTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        addLeftBarButtonItem(#imageLiteral(resourceName: "hamburguer"), target: self, action: #selector(toogleMenus(_:)))
        setupNavigationBarTitleWithImage(#imageLiteral(resourceName: "nav_sumetal"))
        carrinho()
        
        let _: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BaseTableViewController.dismissKeyboard))
//         view.addGestureRecognizer(tap)
        self.tableView.tableFooterView = UIView()
    }
    func toogleMenus(_ sender: UIBarButtonItem){
        myAppDelegate.sideMenuController?.toggleLeftViewAnimated()
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
