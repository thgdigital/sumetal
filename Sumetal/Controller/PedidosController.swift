//
//  PedidosController.swift
//  Sumetal
//
//  Created by Thiago Santos on 16/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class PedidosController: UIViewController {

    @IBOutlet weak var pedidosTableview: UITableView!
    
    
    @IBOutlet weak var dadosPedidosTableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addLeftBarButtonItem(#imageLiteral(resourceName: "hamburguer"), target: self, action: #selector(toogleMenus(_:)))
        setupNavigationBarTitleWithImage(#imageLiteral(resourceName: "nav_sumetal"))
 carrinho()        
        self.dadosPedidosTableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: self.dadosPedidosTableView.frame.size.width, height: 1))
    }

    func toogleMenus(_ sender: UIBarButtonItem){
        myAppDelegate.sideMenuController?.toggleLeftViewAnimated()
    }
    
    // MARK: - Instance
    class func instantiateFromStoryboard() -> PedidosController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "PedidosController") as! PedidosController
    }


}
