//
//  CadastroController.swift
//  Sumetal
//
//  Created by Thiago Santos on 05/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import AKMaskField

class CadastroController: UITableViewController {

    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var cnpjTextField: UITextField!
    
    
    @IBOutlet weak var senhaTextField: UITextField!
    
    
    @IBOutlet weak var endercoTextField: UITextField!
    
    
    @IBOutlet weak var cidadeTextField: UITextField!
    
    @IBOutlet weak var cepTextField: UITextField!
    
    
    @IBOutlet weak var telefoneTextField: UITextField!
    
    
    @IBAction func sendCdastrar(_ sender: Any) {
        
self.navigationController?.view.showActivityView()
        guard let email = emailTextField.text, let username = usernameTextField.text, let cpnj = cnpjTextField.text, let senha = senhaTextField.text , let endereco = endercoTextField.text, let cidade = cidadeTextField.text, let cep = cepTextField.text, let telefone = telefoneTextField.text , email != "", username != "" , cpnj != "", senha != "", endereco != "" , cidade != "", cep != "", telefone != ""   else{
        
            UIAlertController.showAlert(title: "Opss Error", message: "Verificar se exitem dados em brancos ou e-mail invalido.", cancelButtonTitle: "Fechar",  cancelBlock: {
                self.navigationController?.view.hideActivityView()
            })
//            activityIndicatorView.stopAnimating()
        return
            
        }
        var user = User()
        user.userName       = username
        user.email          = email
        user.cep            = cep
        user.cnpj           = cpnj
        user.cidade         = cidade
        user.telefone       = telefone
        
        APIClient.shared.cadastroUser(user: user , completionHandler: { (status, error) in
            defer{
                self.navigationController?.view.hideActivityView()
            }
            if status {
                UIAlertController.showAlert(title: "Dados Salvos", message: "Usuario salvo com sucesso", cancelButtonTitle: "Fechar", viewController: self, cancelBlock: {
                    self.navigationController?.popViewController(animated: true)
                })
                return
            }
     
            UIAlertController.showAlert(title: "Opss Error", message:"Error ao criar conta", cancelButtonTitle: "Fechar",  cancelBlock: nil)
            
        })
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarBackButton()
        setupNavigationBarTitleWithImage(#imageLiteral(resourceName: "nav_sumetal"))
        self.navigationItem.title = "CRIAR CADASTRO"
        self.mask()
          self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 1))
    }

    func mask(){
//    CNPJ: 05.107.205/0001-94
//        cnpjTextField.maskDelegate = self
     
    }
    // MARK: - Instance
    
    class func instantiateFromStoryboard() -> CadastroController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "CadastroController") as! CadastroController
    }
   

}
extension CadastroController: AKMaskFieldDelegate{
    
    
}


