//
//  DatabaseSincronizerController.swift
//  Sumetal
//
//  Created by Thiago Santos on 21/07/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class DatabaseSincronizerController: UITableViewController {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var produtos = [Produto]()
    var errosId : [Int]  = []
    let cdManager = CoreDataManager.sharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()

        addLeftBarButtonItem(#imageLiteral(resourceName: "hamburguer"), target: self, action: #selector(toogleMenus(_:)))
        setupNavigationBarTitleWithImage(#imageLiteral(resourceName: "nav_sumetal"))
       self.tableView.tableFooterView = UIView()
         syncDatase()
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
    }

    @IBAction func sincronizar(_ sender: Any) {
        
        
         self.navigationController?.view.showActivityView(withLabel: "Baixando dados")
        
        
        APIClient.shared.featchProdutoAll { (produtos, error) in
            defer{
                self.navigationController?.view.hideActivityView()
            }
            if let _error = error as NSError?{
                print(_error.localizedDescription)
                return
            }
            
            
            guard let  _produto = produtos else {return}
            self.produtos = _produto
            self.deleteAllProdutos()
            self.saveCoredate(produtos: _produto)
            self.deleteAllClientes()
            self.clientes()
            
            self.tableView.reloadData()
            return
        }
    }
  
    func toogleMenus(_ sender: UIBarButtonItem){
        myAppDelegate.sideMenuController?.toggleLeftViewAnimated()
    }
    func saveCoredate(produtos: [Produto]){
        
        for  _prod in produtos {
            let prod = self.cdManager.new("ProdutoManager") as! ProdutoManager
            prod.dimensao       = _prod.dimensao
            prod.thumb          = _prod.thumb
            prod.acabamento     = _prod.acabamento
            prod.codigo         = _prod.codigo
            prod.categoria      = _prod.categoria
            prod.correia        = _prod.correia
            prod.fabricacao     = _prod.fabricacao
            prod.fixacao        =  _prod.fixacao
            prod.formato        = _prod.formato
            prod.fundo          = _prod.fundo
            prod.id             = Int16(_prod.id)
            prod.modalidade     = _prod.modalidade
            prod.nome           = _prod.nome
            prod.peso           = _prod.peso
            prod.preco          = _prod.preco
            prod.subcategoria   = _prod.subcategoria
            prod.tamanho        = _prod.tamanho
            CoreDataManager.sharedInstance.saveObj(prod)
            
        }
    }
    func syncDatase(){
//        self.navigationController?.view.hideActivityView()
        
        // Create a sort descriptor object that sorts on the "title"
        // property of the Core Data object
        let predicate = NSPredicate(value: true)
        let ordeby = NSSortDescriptor(key: "id", ascending: true)
        
        let cdManager = CoreDataManager.sharedInstance
        guard  let _produto =  cdManager.customSearch("ProdutoManager",  predicate: predicate, sortDescriptor: ordeby) as? [ProdutoManager] else {
            
            return
        }
        produtos = []
            
            for _prod  in _produto{
                var produto = Produto()
                
                produto.acabamento  = _prod.acabamento!
                produto.categoria   = _prod.categoria!
                produto.codigo      = _prod.codigo!
                produto.correia     = _prod.correia!
                produto.dimensao    = _prod.dimensao!
                produto.fabricacao  = _prod.fabricacao!
                produto.fixacao     = _prod.fixacao!
                produto.formato     = _prod.formato!
                produto.fundo       = _prod.fundo!
                produto.id          = Int(_prod.id)
                produto.modalidade  = _prod.modalidade!
                produto.nome        = _prod.nome!
                produto.peso        = _prod.peso!
                produto.tamanho     = _prod.tamanho!
                produto.thumb       = _prod.thumb!
                produtos.append(produto)
                
                
            }
        
            tableView.reloadData()
   }
    func deleteAllProdutos() {
        let predicate = NSPredicate(value: true)
        let ordeby = NSSortDescriptor(key: "id", ascending: true)
       
        
        let lista = cdManager.customSearch("ProdutoManager", predicate: predicate, sortDescriptor: ordeby) as! [ProdutoManager]
        
        if lista.isEmpty{
            
            return
        }
        
        for data in lista {
            cdManager.deleteObj(data)
        }
        
        
    }
    func deleteAllClientes() {
        let predicate = NSPredicate(value: true)
        let ordeby = NSSortDescriptor(key: "cnpj", ascending: true)
        
        
        let lista = cdManager.customSearch("Clientes", predicate: predicate, sortDescriptor: ordeby) as! [Clientes]
        
        if lista.isEmpty{
            
            return
        }
        
        for data in lista {
            cdManager.deleteObj(data)
        }
        
        
    }
    func clientes(){
        APIClient.shared.fetchClientes(user: "") { (user, error) in
          guard let  _user = user else {return}
            for  _users in _user {
                let _cliente = self.cdManager.new("Clientes") as! Clientes
                    _cliente.nome = _users.nome
                    _cliente.cep = _users.cep
                    _cliente.cidade = _users.cidade
                    _cliente.estado = _users.estado
                    _cliente.telefone = _users.telefone
                    _cliente.cnpj = _users.cnpj
                    _cliente.endereco = _users.endereco
                    CoreDataManager.sharedInstance.saveObj(_cliente)
                
            }

        }
    }

    // MARK: - Instance
    class func instantiateFromStoryboard() -> DatabaseSincronizerController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "DatabaseSincronizerController") as! DatabaseSincronizerController
    }
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return produtos.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "teste", for: indexPath)  as! ProdutoCell
       
        cell.produto = produtos[indexPath.row]
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller  = ProdutoDetailsController.instantiateFromStoryboard()
        controller.produto = produtos[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
    }

}
extension DatabaseSincronizerController : DZNEmptyDataSetDelegate, DZNEmptyDataSetSource{
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "logo-place")
    }
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let atrributed = NSAttributedString(string:"Opss Erro !!!", attributes:
            [NSForegroundColorAttributeName: UIColor.lightGray,
             NSFontAttributeName: UIFont.boldSystemFont(ofSize: 24)
            ])
        return atrributed
        
    }
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let atrributed = NSAttributedString(string:"Não foi possivel carregar dados.", attributes:
            [NSForegroundColorAttributeName: UIColor.lightGray,
             NSFontAttributeName: UIFont.systemFont(ofSize: 20)
            ])
        return atrributed
    }
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        let atrributed = NSAttributedString(string:"Tentar Novamente", attributes:
            [NSForegroundColorAttributeName: UIColor.white])
        return atrributed
    }
    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> UIImage! {
        return buttonImageForEmptyState()
    }
    func backgroundColor(forEmptyDataSet scrollView: UIScrollView!) -> UIColor! {
        return .clear
    }
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
      syncDatase()
    }
}
